
springboot实现多数据源案例
> 使用springboot基本上都是项目进行微服务化的拆分，但是需求难免会出现集成多数据源。
> 这里简单采用springboot分包方式实现多数据源

# 目录结构
```text
├─src
│  ├─main
│  │  ├─java
│  │  │  └─com
│  │  │      └─itmck
│  │  │          ├─config       多数据源配置包
│  │  │          ├─dao
│  │  │          │  ├─master    主数据源mapper
│  │  │          │  └─slave     从数据源mapper
│  │  │          ├─entity       实体
│  │  │          └─service      接口
│  │  └─resources
│  │      └─mapper
│  │          ├─master          主数据源mapper.xml
│  │          └─slave           从数据源mapper.xml
│  └─test
│      └─java
│          └─com
│              └─itmck
│                  └─service     测试类
```

# application.yml
```yaml
spring:
  datasource:
    master:
      url: jdbc:mysql://localhost:3306/spring_db?characterEncoding=utf-8&useSSL=false&serverTimezone=GMT%2B8
      driver-class-name: com.mysql.cj.jdbc.Driver
      username: root
      password: 123456
    slave:
      url: jdbc:mysql://localhost:3306/spring_db2?characterEncoding=utf-8&useSSL=false&serverTimezone=GMT%2B8
      driver-class-name: com.mysql.cj.jdbc.Driver
      username: root
      password: 123456
    type: com.zaxxer.hikari.HikariDataSource
    hikari:
      connection-timeout: 30000
      idle-timeout: 30000
      auto-commit: 'true'
      minimum-idle: 5
      maximum-pool-size: 15
      pool-name: HikariCP
      connection-test-query: SELECT 1 FROM DUAL
      max-lifetime: 1800000

mybatis-plus:
  mapper-locations: 'classpath*:mapper/*/*Mapper.xml'
  type-aliases-package: com.itmck.entity
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl

```

# com.itmck.filterconfig.PrimaryDataSourceConfig
```java
package com.itmck.filterconfig;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.incrementer.OracleKeyGenerator;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:07
 **/
@Slf4j
@Configuration
@MapperScan(basePackages = "com.itmck.dao.master", sqlSessionTemplateRef = "primarySqlSessionTemplate")
public class PrimaryDataSourceConfig {


    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.master")
    public DataSourceProperties primaryDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    public DataSource primaryDateSource() {
        return primaryDataSourceProperties()
                .initializeDataSourceBuilder()
                .type(HikariDataSource.class)
                .build();
    }

    @Bean
    @Primary
    public SqlSessionFactory primarySqlSessionFactory() throws Exception {
        MybatisSqlSessionFactoryBean mybatisSqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
        mybatisSqlSessionFactoryBean.setPlugins(new PaginationInterceptor());
        mybatisSqlSessionFactoryBean.setDataSource(primaryDateSource());
//        mybatisSqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/master/*.xml"));
//        mybatisSqlSessionFactoryBean.setConfiguration(this.mybatisConfiguration());//解决oracle下不能使用sequence问题
        return mybatisSqlSessionFactoryBean.getObject();
    }

    private MybatisConfiguration mybatisConfiguration() {
        GlobalConfig globalConfig = new GlobalConfig();
        GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig().setKeyGenerator(new OracleKeyGenerator());
        globalConfig.setDbConfig(dbConfig);
        MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();
        mybatisConfiguration.setGlobalConfig(globalConfig);
        return mybatisConfiguration;
    }


    @Bean
    @Primary
    public DataSourceTransactionManager primaryDataSourceTransactionManager() {
        return new DataSourceTransactionManager(primaryDateSource());
    }

    @Bean
    @Primary
    public SqlSessionTemplate primarySqlSessionTemplate() throws Exception {
        return new SqlSessionTemplate(primarySqlSessionFactory());
    }

}
```
# com.itmck.filterconfig.SlaveDataSourceConfig
```java
package com.itmck.filterconfig;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.incrementer.OracleKeyGenerator;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;


/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:07
 * <p>
 * 从数据源
 **/
@Slf4j
@Configuration
@MapperScan(basePackages = "com.itmck.dao.slave",sqlSessionTemplateRef = "slaveSqlSessionTemplate")
public class SlaveDataSourceConfig {


    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.slave")
    public DataSourceProperties slaveDataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean
    public DataSource slaveDateSource() {
        return slaveDataSourceProperties()
                .initializeDataSourceBuilder()
                .type(HikariDataSource.class)
                .build();
    }

    @Bean
    public SqlSessionFactory slaveSqlSessionFactory() throws Exception {
        MybatisSqlSessionFactoryBean mybatisSqlSessionFactoryBean = new MybatisSqlSessionFactoryBean();
        mybatisSqlSessionFactoryBean.setPlugins(new PaginationInterceptor());
        mybatisSqlSessionFactoryBean.setDataSource(slaveDateSource());
//        mybatisSqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/slave/*.xml"));
//        mybatisSqlSessionFactoryBean.setConfiguration(this.mybatisConfiguration());//解决oracle下不能使用sequence问题
        return mybatisSqlSessionFactoryBean.getObject();
    }

    private MybatisConfiguration mybatisConfiguration() {
        GlobalConfig globalConfig = new GlobalConfig();
        GlobalConfig.DbConfig dbConfig = new GlobalConfig.DbConfig().setKeyGenerator(new OracleKeyGenerator());
        globalConfig.setDbConfig(dbConfig);
        MybatisConfiguration mybatisConfiguration = new MybatisConfiguration();
        mybatisConfiguration.setGlobalConfig(globalConfig);
        return mybatisConfiguration;
    }

    @Bean
    public DataSourceTransactionManager slaveDataSourceTransactionManager() {
        return new DataSourceTransactionManager(slaveDateSource());
    }

    @Bean
    public SqlSessionTemplate slaveSqlSessionTemplate() throws Exception {
        return new SqlSessionTemplate(slaveSqlSessionFactory());
    }
}
```

# 启动类：
```java
package com.itmck;

import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:03
 **/
@SpringBootApplication(
        //排除默认的数据源自动装配配置类
        exclude = {
                DataSourceAutoConfiguration.class,
                DataSourceTransactionManagerAutoConfiguration.class,
                MybatisPlusAutoConfiguration.class
        }
)
@EnableTransactionManagement
public class SpringbootMultipartDataSourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMultipartDataSourcesApplication.class, args);
    }
}
```

**注意事项**
- 配置类上:@MapperScan(basePackages = "com.itmck.dao.master", sqlSessionTemplateRef = "primarySqlSessionTemplate")
> 其中：basePackages代表要扫描的mapper接口，这是分包切换数据源的关键
- 启动类上:@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class,DataSourceTransactionManagerAutoConfiguration.class,MybatisPlusAutoConfiguration.class})
> 使用 exclude 排除对应的自动装配配置