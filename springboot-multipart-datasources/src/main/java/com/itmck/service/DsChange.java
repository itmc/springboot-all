package com.itmck.service;

import com.itmck.entity.Student;
import com.itmck.entity.User;

import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:49
 **/
public interface DsChange {
    List<User> getUserList();

    List<Student> getStudentList();
}