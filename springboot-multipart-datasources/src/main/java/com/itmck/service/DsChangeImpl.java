package com.itmck.service;

import com.baomidou.mybatisplus.extension.service.additional.query.impl.LambdaQueryChainWrapper;
import com.itmck.dao.master.UserMapper;
import com.itmck.dao.slave.StudentMapper;
import com.itmck.entity.Student;
import com.itmck.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:49
 **/
@Service
public class DsChangeImpl implements DsChange {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private StudentMapper studentMapper;

    @Override
    public List<User> getUserList() {
        return new LambdaQueryChainWrapper<>(userMapper).list();
    }

    @Override
    public List<Student> getStudentList() {
        return new LambdaQueryChainWrapper<>(studentMapper).list();
    }
}