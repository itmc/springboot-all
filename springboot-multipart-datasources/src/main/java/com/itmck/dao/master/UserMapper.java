package com.itmck.dao.master;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * 太阳当空照,花儿对我笑
 * 使用Mybatis-plus通用mapper
 * Create by M ChangKe 2021/11/16 9:39
 **/
@Mapper
public interface UserMapper extends BaseMapper<User> {


}
