package com.itmck;

import com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:03
 **/
@SpringBootApplication(
        //排除默认的数据源自动装配配置类
        exclude = {
                DataSourceAutoConfiguration.class,
                DataSourceTransactionManagerAutoConfiguration.class,
                MybatisPlusAutoConfiguration.class
        }
)
@EnableTransactionManagement
public class SpringbootMultipartDataSourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMultipartDataSourcesApplication.class, args);
    }
}