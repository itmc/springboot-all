package com.itmck.exception;

import com.itmck.component.ApiResultResponse;
import org.apache.shiro.ShiroException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


/**
 * 统一捕捉shiro的异常，返回给前台一个json信息，前台根据这个信息显示对应的提示，或者做页面的跳转。
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    //不满足@RequiresGuest注解时抛出的异常信息
    private static final String GUEST_ONLY = "Attempting to perform a guest-only operation";


    @ExceptionHandler(ShiroException.class)
    @ResponseBody
    public ApiResultResponse<String>  handleShiroException(ShiroException e) {
        String eName = e.getClass().getSimpleName();
        log.error("shiro执行出错：{}", eName);
        return ApiResultResponse.error("鉴权或授权过程出错", "500");
    }

    @ExceptionHandler(UnauthenticatedException.class)
    @ResponseBody
    public ApiResultResponse<String>  page401(UnauthenticatedException e) {
        String eMsg = e.getMessage();
        if (StringUtils.startsWithIgnoreCase(eMsg, GUEST_ONLY)) {
            return ApiResultResponse.error("只允许游客访问，若您已登录，请先退出登录", "401");
        } else {
            return ApiResultResponse.error("用户未登录", "401");
        }
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseBody
    public ApiResultResponse<String>  page403() {

        return ApiResultResponse.error("用户没有访问权限", "403");
    }

}