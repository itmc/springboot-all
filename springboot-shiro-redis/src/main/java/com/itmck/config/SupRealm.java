package com.itmck.config;

import com.itmck.pojo.User;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

import java.util.HashSet;
import java.util.Set;

public class SupRealm extends AuthorizingRealm {
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        Set<String> roles = new HashSet<>();
        roles.add("role1");
        roles.add("role2");
        simpleAuthorizationInfo.addRoles(roles);
        
        Set<String> perms = new HashSet<>();
        perms.add("perms1");
        perms.add("perms2");
        simpleAuthorizationInfo.addStringPermissions(perms);
        return simpleAuthorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken)authenticationToken;
        String userName = token.getUsername();
        String pwd = String.valueOf(token.getPassword());

        User userInfo = new User();
        userInfo.setPassword(pwd);
        userInfo.setUserName(userName);

        return new SimpleAuthenticationInfo(userInfo.getUserName(), userInfo.getPassword(), "SupRealm");
    }
}

