package com.itmck;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * https://blog.csdn.net/c_z_z/article/details/116454789
 */
@SpringBootApplication
public class SpringbootShiroRedis {
    public static void main(String[] args) {

        SpringApplication.run(SpringbootShiroRedis.class,args);
    }
}
