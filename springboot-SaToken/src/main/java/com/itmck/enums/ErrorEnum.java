package com.itmck.enums;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/3 10:26
 **/
public enum ErrorEnum {

    ERROR_500("500","系统异常");

    private  final String code;

    private final String description;

    ErrorEnum(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}