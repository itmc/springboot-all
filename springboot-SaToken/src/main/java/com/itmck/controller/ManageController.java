package com.itmck.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/manage/")
public class ManageController {

    // 测试登录，浏览器访问： http://localhost:8081/manage/info?username=zhang
    @GetMapping("/info")
    public String doLogin(String username) {
        return "hello :" + username;
    }

}
