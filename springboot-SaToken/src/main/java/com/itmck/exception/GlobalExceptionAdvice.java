package com.itmck.exception;

import cn.dev33.satoken.exception.NotLoginException;
import com.itmck.pojo.resp.ApiResultResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/3 10:33
 * <p>
 * 自定义全局异常处理器
 **/
@Slf4j
@RestControllerAdvice
public class GlobalExceptionAdvice {


    @ExceptionHandler(value = NotLoginException.class)
    public ApiResultResponse<String> notLoginExceptionHandler(NotLoginException exception) {
        log.error("未登录异常:{}", exception.getMessage());
        return ApiResultResponse.error(exception.getMessage(), "500");
    }

    @ExceptionHandler(value = NullPointerException.class)
    public ApiResultResponse<String> responseException(NullPointerException exception) {
        log.error("空指针异常", exception);
        return ApiResultResponse.error("空指针异常", "500");
    }


    @ExceptionHandler(value = BusinessException.class)
    public ApiResultResponse<String> businessHandler(BusinessException exception) {
        log.error("业务异常", exception);
        return ApiResultResponse.error(exception.getMessage(), exception.getCode());
    }

    @ExceptionHandler(value = Exception.class)
    public ApiResultResponse<String> defaultHandler(Exception exception) {
        log.error("系统异常", exception);
        return ApiResultResponse.error("空指针异常", "500");
    }
}