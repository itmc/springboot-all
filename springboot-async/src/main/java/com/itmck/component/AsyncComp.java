package com.itmck.component;


import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class AsyncComp {


    @Async("myExecutor")
    public void asyncMethod() {

       log.info("执行asyncMethod方法");

    }
}
