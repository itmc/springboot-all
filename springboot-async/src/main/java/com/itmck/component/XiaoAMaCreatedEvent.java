package com.itmck.component;

import org.springframework.context.ApplicationEvent;

/**
 * [说明]
 *
 * @author xiaoama
 */
public class XiaoAMaCreatedEvent extends ApplicationEvent {

    private String message;

    public XiaoAMaCreatedEvent(String message) {
        super(message);
        this.message=message;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
