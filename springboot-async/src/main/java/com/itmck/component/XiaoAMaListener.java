package com.itmck.component;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * [说明]
 *
 * @author xiaoama
 */
@Component
public class XiaoAMaListener {

    @EventListener
    public void onApplicationEvent(XiaoAMaCreatedEvent event) {
        String message = event.getMessage();
        System.out.println("接受到acceount创建，正在发送创建信息："+message);
    }
}
