package com.itmck.controller;


import com.itmck.component.AsyncComp;
import com.itmck.component.XiaoAMaCreatedEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@Slf4j
@RestController
public class AsyncController {


    @Resource
    private AsyncComp asyncComp;

    @Resource
    private ApplicationEventPublisher applicationEventPublisher;


    @GetMapping("/hello")
    public String hello() {

        asyncComp.asyncMethod();
        log.info("response hello");
        return "hello";
    }

    @GetMapping("/hello2")
    public String hello2() {

        applicationEventPublisher.publishEvent(new XiaoAMaCreatedEvent("123"));
        return "hello";
    }
}
