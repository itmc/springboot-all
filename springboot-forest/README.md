# Forest极简http调用框架
# 文档地址:https://forest.dtflyx.com/docs/

# 什么是 Forest
> Forest 是一个开源的 Java HTTP 客户端框架，它能够将 HTTP 的所有请求信息（包括 URL、Header 以及 Body 等信息）绑定到您自定义的 Interface 方法上，能够通过调用本地接口方法的方式发送 HTTP 请求。

# 为什么使用 Forest
> 使用 Forest 就像使用类似 Dubbo 那样的 RPC 框架一样，只需要定义接口，调用接口即可，不必关心具体发送 HTTP 请求的细节。同时将 HTTP 请求信息与业务代码解耦，方便您统一管理大量 HTTP 的 URL、Header 等信息。而请求的调用方完全不必在意 HTTP 的具体内容，即使该 HTTP 请求信息发生变更，大多数情况也不需要修改调用发送请求的代码。


# 依赖
```xml
<dependency>
    <groupId>com.dtflys.forest</groupId>
    <artifactId>forest-spring-boot-starter</artifactId>
    <version>1.5.13</version>
</dependency>
```

# 简单使用案例
```java
@BaseRequest(baseURL = "http://localhost:8080")//@BaseRequest(baseURL = "${myUrl}")
public interface MySimpleClient {
  
    //get请求无参 http://localhost:8080/getString
    @Get("/getString")
    String getString();
    
    //get请求有参 http://localhost:8080/getString?name=XXX
    @Get("/getString")
    String getString(@Query("name") String name);

    //get请求拼接参数返回自定义响应体
    @Get("/getStringApi")
    ApiResultResponse<String> getStringApi(@Query("name") String name);

    //post请求发送json返回自定义类型
    @Post(url = "/postJson")
    ApiResultResponse<User> postJson(@JSONBody User user);
}
```
举例说明:
```text
> @BaseRequest进行url的抽取,配置后,同一个接口类下使用同样的url路经.
> @Address(host = "localhost",port = "8080") 可以设置host与端口.
```

说明:其中路径可以使用${}进行注入,如@BaseRequest(baseURL = "${myUrl}"),需要配置application.yml
```yaml
forest:
  variables:
    myUrl: http://localhost:8080
```
如下请求:

##  Get请求+参数 说明:
```text
@Get("/getString")
String getString(@Query("name") String name);

@Query:参数会拼接在url后边


GET http://localhost:8080/getStringApi?name=mck HTTP
[Forest] Response: Status = 200, Time = 46ms
result:ApiResultResponse(message=ok, errorCode=200, success=true, result=mck)
```
## Post请求发送json返回自定义类型说明

```text

@Post(url = "/postJson")
ApiResultResponse<User> postJson(@JSONBody User user);

@JSONBody 发送Json数据,ApiResultResponse为自定义数据类型,框架会自动识别将数据转换成自定义类型


POST http://localhost:8080/postJson HTTP
Headers: 
    Content-Type: application/json
Body: {"age":26,"name":"mck"}
[Forest] Response: Status = 200, Time = 195ms
result:ApiResultResponse(message=ok, errorCode=200, success=true, result=User(name=mck, age=26))

```


# 异常处理
```java

@BaseRequest(baseURL = "http://localhost:8080")
public interface MySimpleClient {
    //===========================异常处理方式======================

    /**
     * ForestResponse:
     * 直接用普通的对象类型作为请求方法的返回类型，可以将响应数据方便的反序列化，以满足大部分的需求。
     * 但还有很多时候不光需要获取响应内容，也需要得到响应头等信息，这时候就需要 ForestResponse
     * http://localhost:8080/getString?name=XXX
     */
    @Get("/getString")
    ForestResponse<String> getForestResponse(@Query("name") String name);

    /**
     * 在请求接口中定义OnError回调函数类型参数
     */
    @Get("/getStringApi")
    ApiResultResponse<String> send(@Query("name") String name, OnError onError);
}

```

# 测试
```java
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyClientTest {
    @Resource
    private MySimpleClient mySimpleClient;

    @Test
    public void getString() {
        String result = mySimpleClient.getString();
        log.info("result:{}", result);
    }

    @Test
    public void getStringApi() {

        ApiResultResponse<String> result = mySimpleClient.getStringApi("mck");
        log.info("result:{}", result);

    }


    @Test
    public void postJson() {

        User user = new User();
        user.setName("mck");
        user.setAge(26);
        ApiResultResponse<User> result = mySimpleClient.postJson(user);
        log.info("result:{}", result);
    }


    @Test
    public void getForestResponse() {

        ForestResponse<String> result = mySimpleClient.getForestResponse("mck");
        if (result.isSuccess()) {
            log.info("result:{}", result.getStatusCode());
            log.info("result:{}", result.getContentType());
            log.info("result:{}", result.getContent());
            log.info("result:{}", result.getResult());
        }

    }

    @Test
    public void run() {
        // 在调用接口时，在Lambda中处理错误结果
        ApiResultResponse<String> foo = mySimpleClient.send("foo", (ex, request, response) -> {
            int status = response.getStatusCode(); // 获取请求响应状态码
            log.info("status:{}",status);
            String content = response.getContent(); // 获取请求的响应内容
            log.info("content:{}",content);
            String result = (String) response.getResult(); // 获取方法返回类型对应的最终数据结果
            log.info("result:{}",result);
        });
        log.info("foo:{}", foo);
    }
}
```