package com.itmck.request;


import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class OcrDomain {

    private String license;
    private String image;
    private String plugin;
    private String angle;
}
