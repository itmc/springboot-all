package com.itmck.response;

import lombok.Data;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 9:38
 **/
@Data
public class User {

    private String name;

    private int age;
}