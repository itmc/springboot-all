package com.itmck.client;

import com.dtflys.forest.annotation.*;
import com.dtflys.forest.callback.OnError;
import com.dtflys.forest.http.ForestResponse;
import com.itmck.response.ApiResultResponse;
import com.itmck.response.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 9:03
 * Forest Get,Post请求测试案例
 **/
@BaseRequest(baseURL = "${myUrl}")
//@Address(host = "localhost",port = "8080")
public interface MySimpleClient {


    /**
     * get请求无参
     * http://localhost:8080/getString
     */
    @Get("/getString")
    String getString();

    /**
     * get请求有参
     * http://localhost:8080/getString?name=XXX
     */
    @Get("/getString")
    String getString(@Query("name") String name);

    //get请求拼接参数返回自定义响应体
    @Get("/getStringApi")
    ApiResultResponse<String> getStringApi(@Query("name") String name);


    /**
     * post请求发送json返回自定义类型
     */
    @Post(url = "/postJson")
    ApiResultResponse<User> postJson(@JSONBody User user);


    //===========================异常处理方式======================

    /**
     * ForestResponse:
     * 直接用普通的对象类型作为请求方法的返回类型，可以将响应数据方便的反序列化，以满足大部分的需求。
     * 但还有很多时候不光需要获取响应内容，也需要得到响应头等信息，这时候就需要 ForestResponse
     * http://localhost:8080/getString?name=XXX
     */
    @Get("/getString")
    ForestResponse<String> getForestResponse(@Query("name") String name);

    /**
     * 在请求接口中定义OnError回调函数类型参数
     */
    @Get("/getStringApi")
    ApiResultResponse<String> send(@Query("name") String name, OnError onError);

    //================调用远程接口进行文件的上传操作=================================

    /**
     * 上传文件
     *
     * @param file 文件
     */
    @Post("/upload1")
    void uploadFile(@DataFile(value = "file") MultipartFile file);


    @Post("/upload")
    void uploadFile(@DataFile(value = "file") File file);


    //=================指定文件地址与文件名进行下载操作===============


    @GetRequest("download")
//    @DownloadFile(dir="D:\\mck",filename = "123.xlsx")
    InputStream downLoadFile(@Query("fileName") String fileName);
}