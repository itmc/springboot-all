package com.itmck.client;

import com.dtflys.forest.annotation.*;
import com.itmck.request.OcrDomain;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 9:03
 * Forest Get,Post请求测试案例
 **/
@BaseRequest(baseURL = "")
public interface MySimpleClient2 {


    /**
     * post请求发送json返回自定义类型
     */
    @Post(url = "/server/common")
    String postJson(@JSONBody OcrDomain user);


}