package com.itmck.config;


import lombok.Data;

@Data
public class OcrBusinessVO {

    /**
     * 注册号/统一社会信用代码
     */

    @OcrMethod("注册号/统一社会信用代码")
    private String socialCreditCode;

    /**
     * 名称
     */
    @OcrMethod("名称")
    private String name;

    /**
     * 类型
     */
    @OcrMethod("类型")
    private String type;

    /**
     * 注册资本
     */
    @OcrMethod("注册资本")
    private String registeredCapital;


    /**
     * 组成形式
     */
    @OcrMethod("组成形式")
    private String formComposition;


    /**
     * 住所/经营场所
     */
    @OcrMethod("住所/经营场所")
    private String address;

    /**
     * 经营者/法定代表人/投资人
     */
    @OcrMethod("经营者/法定代表人/投资人")
    private String operators;

    /**
     * 经营期限/营业期限
     */
    @OcrMethod("经营期限/营业期限")
    private String termOperation;

    /**
     * 成立日期/注册日期
     */
    @OcrMethod("成立日期/注册日期")
    private String beginDate;

}
