package com.itmck.config;

import cn.hutool.core.util.ReflectUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.util.Map;


@Slf4j
public class OcrDealUtil {
    public static <T> T delResult(String result, Class<T> clazz) {
        Map<String,Object> map = JSON.parseObject(result);
        T object = ReflectUtil.newInstance(clazz);
        Field[] fields = ReflectUtil.getFields(clazz);
        for (Field field : fields) {
            if (field.isAnnotationPresent(OcrMethod.class)) {
                OcrMethod annotation = field.getAnnotation(OcrMethod.class);
                String key = annotation.value();
                if (map.containsKey(key)) {
                    Object value = map.get(key);
                    ReflectUtil.setFieldValue(object, field, value);
                }
            }
        }
        return object;
    }

    public static void main(String[] args) {

        String json = "{\n" +
                "  \"注册号/统一社会信用代码\": \"AAAAAAAAAA\",\n" +
                "  \"名称\": \"BBBBBBBBBB\",\n" +
                "  \"类型\": \"CCCCCCCCCC\",\n" +
                "  \"注册资本\": \"DDDDDDDDDD\",\n" +
                "  \"组成形式\": \"EEEEEEEEEE\",\n" +
                "  \"住所/经营场所\": \"FFFFFFFFFF\",\n" +
                "  \"经营者/法定代表人/投资人\": \"GGGGGGGGGG\",\n" +
                "  \"经营期限/营业期限\": \"HHHHHHHHHH\",\n" +
                "  \"成立日期/注册日期\": \"IIIIIIIIII\"\n" +
                "}";
        OcrBusinessVO ocrBusinessVO = OcrDealUtil.delResult(json, OcrBusinessVO.class);
        System.out.println(ocrBusinessVO);

    }
}
