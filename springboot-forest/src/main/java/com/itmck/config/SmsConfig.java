package com.itmck.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 短信发送配置
 *
 * @author liumeng
 * @date 2021-07-28
 */
@Data
@Configuration
public class SmsConfig {


    /**
     * 是否开启ocr
     */
    @Value("${ocr.ocr_open:false}")
    private Boolean crOpen;


}
