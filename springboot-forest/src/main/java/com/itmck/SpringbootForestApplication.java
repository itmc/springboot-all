package com.itmck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 9:17
 **/
@EnableSwagger2
@SpringBootApplication
public class SpringbootForestApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringbootForestApplication.class, args);
    }
}