package com.itmck.client;

import cn.hutool.core.codec.Base64;
import com.dtflys.forest.http.ForestResponse;
import com.itmck.request.OcrDomain;
import com.itmck.response.ApiResultResponse;
import com.itmck.response.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.File;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 9:41
 **/

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyClientTest {

    @Resource
    private MySimpleClient mySimpleClient;

    @Resource
    private MySimpleClient2 mySimpleClient2;


    @Test
    public void ocr() {

        String encode = Base64.encode(new File("/Users/mchangke/Downloads/WechatIMG140.jpeg"));

        String license="GABkUHZydFM4eXJmTHhpd0tuM3pVV3B3PT2sADZMTlBlRDlNb1BDZUphVFVYNG9KS2Jwc0NOaEtuWExyeUJqVUJ4UmtKZW42dlVZdi85ZEpISlZYaGxySG9tdXlqUjZNK0xmdjVyOVNqdnUyTWpsNTkybk93TXA4YVQ1Z3EwZkVPWEtCbytuMVNicDA5TGZDeWswRHpzTHdLL0hBSEl1aHdsVVZuY3lDTXYzd1pmVmZubFNjc1JQeGM2Rk93VjJzeS9pekt3WT1IRWtKRmF3TTNkWmtGdXNZRGtDeFlHVXd0SGtjRHJuMjV3Z1IrMHk0RmJoYnZXZE8weFdWZlhhVHMwajJZM3ZBS1ROQ2NXQXpSWjBkc2VTclUrRnhpdmgwUWZGeWlBWDBnN0tWWTZUM0hmZENvUEFBK0x6VnBWdlhSYzZCa2NTZVhLOVJyVEJYNStwUStYMi9BZmswTE1SWFZ0enZzems4bzRVUkExK0tQcEExZW12U08vMnE3N3Q4ZlJiWGRMNmw5ZS9rK0pyMlExL0Z2VFpUVHAyeWl4K3oxcHNnOVJvM1JBaUFaZy8rQ2JtQjFDRG4xZUpRVExBZVE1VUdMcWpJL1BUTnIvaVI0US9rdFhBOXVyaU1tdlowUnNqUVcydHFPR0IzcnFEdUJqNjhyZ25GVlFjMDJxVmwwRURnRFpxbkpRWVdUeU0vL3N5WGFtZ1BSSmM0amc9PQ==";

        OcrDomain ocrDomain= new OcrDomain();
        ocrDomain.setLicense(license)
                .setImage(encode)
                .setPlugin("common");
        String result = mySimpleClient2.postJson(ocrDomain);

        log.info("result:{}", result);
    }


    @Test
    public void getString() {
        String result = mySimpleClient.getString();
        log.info("result:{}", result);
    }

    @Test
    public void getStringApi() {

        ApiResultResponse<String> result = mySimpleClient.getStringApi("mck");
        log.info("result:{}", result);

    }


    @Test
    public void postJson() {

        User user = new User();
        user.setName("mck");
        user.setAge(26);
        ApiResultResponse<User> result = mySimpleClient.postJson(user);
        log.info("result:{}", result);
    }


    @Test
    public void getForestResponse() {

        ForestResponse<String> result = mySimpleClient.getForestResponse("mck");
        if (result.isSuccess()) {
            log.info("result:{}", result.getStatusCode());
            log.info("result:{}", result.getContentType());
            log.info("result:{}", result.getContent());
            log.info("result:{}", result.getResult());
        }

    }

    @Test
    public void run() {
        // 在调用接口时，在Lambda中处理错误结果
        ApiResultResponse<String> foo = mySimpleClient.send("foo", (ex, request, response) -> {
            int status = response.getStatusCode(); // 获取请求响应状态码
            log.info("status:{}",status);
            String content = response.getContent(); // 获取请求的响应内容
            log.info("content:{}",content);
            String result = (String) response.getResult(); // 获取方法返回类型对应的最终数据结果
            log.info("result:{}",result);
        });
        log.info("foo:{}", foo);
    }

    @Test
    public void upload(){

        File file = new File("D:\\pmp\\ITTO.xlsx");
        mySimpleClient.uploadFile(file);

    }
}