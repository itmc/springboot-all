package com.itmck.client;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 9:41
 **/

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyClientTest2 {



    @Value("${ocr.ocr_open:true}")
    private Boolean open;


    @Test
    public void ocr() {

        if (open)
            System.out.println(">>>>>>"+open);


    }

}