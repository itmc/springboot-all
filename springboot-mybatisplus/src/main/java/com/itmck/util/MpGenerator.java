package com.itmck.util;

import cn.hutool.setting.Setting;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * https://baomidou.com/pages/981406/#entity-%E7%AD%96%E7%95%A5%E9%85%8D%E7%BD%AE
 */
public class MpGenerator {

    public static void main(String[] args) {
        String username = "root";
        String password = "root123456";
        String url = "jdbc:mysql://localhost:3306/yx?characterEncoding=utf-8&useSSL=false&serverTimezone=GMT%2B8";


        String author = "itmck";//作者
        String outputDir = "/Users/mchangke/Documents/mp";
        String parent = "com.itmck";
        String moduleName = "";//包名
        String controller = "controller";//Controller 包名
        String entity = "entity";//Entity 包名
        String mapper = "mapper";//Mapper XML 包名
        String mapper_xml = "/Users/mchangke/Documents/mp/mybatis";//设置mapperXml生成路径

        List<String> tableList = new ArrayList<>(new Setting("tables.setting").getMap("tb").keySet());

        FastAutoGenerator.create(url, username, password)
                .globalConfig(builder -> {
                    builder.author(author) // 设置作者
//                            .enableSwagger() // 开启 swagger 模式
                            .outputDir(outputDir)// 指定输出目录
                            .disableOpenDir()
                            .build()
                    ;
                })
                .packageConfig(builder -> {
                    builder.parent(parent) // 设置父包名
                            .moduleName(moduleName) // 设置父包模块名
                            .controller(controller)//Controller 包名
                            .entity(entity)//Entity 包名
                            .xml(mapper)//Mapper XML 包名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, mapper_xml))// 设置mapperXml生成路径
                            .build()
                    ;
                })
                .strategyConfig(builder -> builder
                                .addInclude(tableList)//设置需要生成的表名
                                .addTablePrefix("mb_")//设置过滤表前缀
                                .controllerBuilder()
                                .enableHyphenStyle()//开启驼峰转连字符
                                .enableRestStyle()//开启生成@RestController 控制器
                                .formatFileName("%sController")//格式化文件名称
                                .serviceBuilder()
                                .formatServiceFileName("%sService")
                                .formatServiceImplFileName("%sServiceImp")
                                .mapperBuilder()
                                .enableMapperAnnotation()
                                .enableBaseResultMap()
                                .enableBaseColumnList()
                                .formatMapperFileName("%sMapper")
                                .formatXmlFileName("%sMapper")
                                .entityBuilder()
                                .disableSerialVersionUID()
                                .enableChainModel()
                                .enableLombok()
                                .enableRemoveIsPrefix()
                                .enableTableFieldAnnotation()
                                .enableActiveRecord()
                                .versionColumnName("version")
                                .versionPropertyName("version")
                                .logicDeleteColumnName("deleted")
                                .logicDeletePropertyName("deleteFlag")
                                .naming(NamingStrategy.underline_to_camel)
                                .columnNaming(NamingStrategy.underline_to_camel)
//                        .addSuperEntityColumns("id", "created_by", "created_time", "updated_by", "updated_time")
//                        .addIgnoreColumns("age")
//                        .addTableFills(new Column("create_time", FieldFill.INSERT))
//                        .addTableFills(new Property("updateTime", FieldFill.INSERT_UPDATE))
                                .idType(IdType.AUTO)
//                                .formatFileName("%sEntity")
                                .build()
                )
                .templateConfig(builder -> {
                    builder.disable(TemplateType.ENTITY, TemplateType.CONTROLLER, TemplateType.SERVICE, TemplateType.SERVICEIMPL)//禁用模板类型
                            .entity("/templates/entity.java")
                            .service("/templates/service.java")
                            .serviceImpl("/templates/serviceImpl.java")
                            .mapper("/templates/mapper.java")
                            .xml("/templates/mapper.xml")//设置 mapperXml 模板路径
                            .controller("/templates/controller.java")
                            .build();

                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
