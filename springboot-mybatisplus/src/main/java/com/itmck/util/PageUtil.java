package com.itmck.util;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itmck.vo.PageBodyResponse;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 14:24
 **/
public class PageUtil {


    public static <T,V> PageBodyResponse<T> covertRecordType(Page<V> page, Class<T> clazz) {

        if (ObjectUtil.isNull(page)) {
            return PageBodyResponse.ok(null, 0);
        }
        List<T> collect = page.getRecords()
                .stream()
                .map(item -> BeanUtil.copyProperties(item, clazz))
                .collect(Collectors.toList());
        return PageBodyResponse.ok(collect, page.getTotal());
    }
}