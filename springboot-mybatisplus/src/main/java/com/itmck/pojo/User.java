package com.itmck.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.itmck.interceptor.annotation.DecryptTransaction;
import com.itmck.interceptor.annotation.EncryptTransaction;
import com.itmck.interceptor.annotation.SensitiveData;
import lombok.Data;

import java.io.Serializable;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/15 16:14
 **/
@Data
@TableName(value = "user")
@SensitiveData
public class User implements Serializable {


    private static final long serialVersionUID = -1205226416664488559L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @DecryptTransaction
    @EncryptTransaction
    private String name;



    private Integer age;

    @DecryptTransaction
    @EncryptTransaction
    private String email;

    @DecryptTransaction
    @EncryptTransaction
    private String cityCode;
}