package com.itmck.page;
 
import java.util.List;
 
/**
 * @version 1.0.0
 * @Description: #分页参数对象
 * @Date: 2021/10/30 11:58
 * @Copyright (C) ZhuYouBin
 */
public class PageVo {
    private Integer pageIndex;
    private Integer pageSize;
    private Integer pages;
    private Integer total;
    private List data;
 
    public PageVo(Integer pageIndex, Integer pageSize) {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }
 
    public Integer getPageIndex() {
        return pageIndex;
    }
 
    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }
 
    public Integer getPageSize() {
        return pageSize;
    }
 
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
 
    public Integer getPages() {
        return pages;
    }
 
    public void setPages(Integer pages) {
        this.pages = pages;
    }
 
    public Integer getTotal() {
        return total;
    }
 
    public void setTotal(Integer total) {
        this.total = total;
    }
 
    public List getData() {
        return data;
    }
 
    public void setData(List data) {
        this.data = data;
    }
 
    @Override
    public String toString() {
        return "PageVo{" +
                "\n  pageIndex=" + pageIndex +
                "\n, pageSize=" + pageSize +
                "\n, pages=" + pages +
                "\n, total=" + total +
                "\n, data=" + data +
                "\n}";
    }
}