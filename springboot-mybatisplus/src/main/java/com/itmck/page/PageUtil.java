package com.itmck.page;
 
/**
 * @version 1.0.0
 * @Description: #分页工具类
 * @Date: 2021/10/30 12:03
 * @Copyright (C) ZhuYouBin
 */
public class PageUtil {
    private static final ThreadLocal<PageVo> LOCAL_PAGE = new ThreadLocal<PageVo>();
 
    // 分页开始对象,设置PageVo
    public static PageVo start(PageVo pageVo) {
        LOCAL_PAGE.set(pageVo);
        return pageVo;
    }
 
    // 分页结束对象,可以获取带有结果集的PageVo
    public static PageVo end() {
        PageVo pageVo = PageUtil.getPageVo();
        LOCAL_PAGE.remove();
        return pageVo;
    }
 
    // 获取分页参数对象
    public static PageVo getPageVo() {
        return LOCAL_PAGE.get();
    }
}