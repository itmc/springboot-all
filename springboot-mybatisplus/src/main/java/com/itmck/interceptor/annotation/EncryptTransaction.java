package com.itmck.interceptor.annotation;

import java.lang.annotation.*;

/**
 * 加密注解
 */
@Documented
@Inherited
@Target({ElementType.FIELD  })
@Retention(RetentionPolicy.RUNTIME)
public @interface EncryptTransaction {

}
