package com.itmck.interceptor.annotation;


import java.lang.annotation.*;


/**
 * 解密注解
 */
@Documented
@Inherited
@Target({ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DecryptTransaction {

}
