package com.itmck.interceptor;

import java.lang.reflect.Field;

public interface DesEncryptFactory {


    /**
     * 加密
     *
     * @param declaredFields
     * @param paramsObject
     * @param <T>
     * @return
     * @throws IllegalAccessException
     */
    <T> T encrypt(Field[] declaredFields, T paramsObject);

    /**
     * 解密
     *
     * @param result resultType的实例
     * @return T
     * @throws IllegalAccessException 字段不可访问异常
     */
    <T> T decrypt(T result);
}
