package com.itmck.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itmck.dao.UserMapper;
import com.itmck.pojo.User;
import com.itmck.vo.PageBodyResponse;
import com.itmck.vo.UserDTO;
import com.itmck.util.MyQueryPageUtil;
import com.itmck.util.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * Create by it_mck 2019/9/14 21:09
 *
 * @Description:
 * @Version: 1.0
 */

@Slf4j
@Service
public class UserServiceImpl implements UserService {




    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public int addRequiresNew() {

//        User user = new User();
//        user.setName("wxp");
//        user.setAge(22);
//        return userMapper.insert(user);

        boolean result = new LambdaUpdateChainWrapper<>(userMapper)
                .eq(User::getName, "mck")
                .set(User::getAge, 29)
                .update();
        int i=1/0;
        return 1;
    }

    @Resource
    private UserMapper userMapper;


    @Override
    public PageBodyResponse<UserDTO> loadPageList(HttpServletRequest request) {
        Page<User> page = MyQueryPageUtil.builder(request, this.userMapper);
        return PageUtil.covertRecordType(page, UserDTO.class);
    }

    @Override
    public List<User> getLists() {

        List<User> users = userMapper.selectList(null);
        log.info("查询结果{}", users);
        return users;
    }


    @Override
    public User findById(Long id) {

        return userMapper.selectById(id);
    }

    /**
     * SELECT id,name,email,age FROM user WHERE name = ? AND email = ?
     * 使用map拼接条件,其中key对应数据库中的字段
     *
     * @param user
     * @return
     */
    @Override
    public List<User> findByCondition(User user) {

        return null;
    }

    /**
     * SELECT id,name,email,age FROM user WHERE (name = ? AND email = ?)
     *
     * @param user
     * @return
     */
    @Override
    public List<User> findByCondition2(User user) {
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", user.getName());
        map.put("email", user.getEmail());
        map.put("age", user.getAge());
        map.put("id", user.getId());
        /**
         * 参数1:Map  参数2:boolean=true(不忽略为null的),false(忽略为null的字段)
         */
        userQueryWrapper.allEq(map, false);
        List<User> users = userMapper.selectList(userQueryWrapper);
        return users;
    }

    @Override
    public User selectById(Long id) {
        return null;
    }
}
