package com.itmck.service;


import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.itmck.dao.UserMapper;
import com.itmck.pojo.User;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class UserService2Impl implements UserService2 {


    @Resource
    private UserMapper userMapper;

    @Override
    public int addRequiresNew() {

//        User user = new User();
//        user.setName("mck");
//        user.setAge(19);
//        return userMapper.insert(user);

        boolean result = new LambdaUpdateChainWrapper<>(userMapper)
                .eq(User::getName, "wxp")
                .set(User::getAge, 30)
                .update();
        return 1;
    }
}
