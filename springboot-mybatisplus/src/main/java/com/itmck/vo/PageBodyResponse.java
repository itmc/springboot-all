package com.itmck.vo;

import lombok.Data;

import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 14:12
 **/
@Data
public class PageBodyResponse<T> {


    /**
     * 当前页数据
     */
    private List<T> list;

    /**
     * 总数
     */
    private long total;

    /**
     * 错误信息
     */
    private String message;

    /**
     * 错误码
     */
    private String errorCode;


    /**
     * 是否成功
     */
    private boolean success = true;


    public static <T> PageBodyResponse<T> ok(List<T> list, long total) {
        PageBodyResponse<T> response = new PageBodyResponse<>();
        response.setList(list);
        response.setTotal(total);
        response.setSuccess(true);
        response.setErrorCode("200");
        return response;
    }

}