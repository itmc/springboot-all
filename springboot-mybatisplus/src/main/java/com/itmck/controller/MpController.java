package com.itmck.controller;

import com.itmck.vo.PageBodyResponse;
import com.itmck.vo.UserDTO;
import com.itmck.service.CommonService;
import com.itmck.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 13:58
 **/
@Slf4j
@RestController
public class MpController {

    @Resource
    private UserService userService;

    @Resource
    private CommonService commonService;

    @GetMapping("mpJson")
    public PageBodyResponse<UserDTO> loadPageList(HttpServletRequest request) {
        return userService.loadPageList(request);
    }

    @GetMapping("is")
    public void insert(){
       commonService.update();
    }
}