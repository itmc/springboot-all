package com.itmck.controller;

import com.itmck.vo.UserParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 太阳当空照,花儿对我笑
 *
 * Create by M ChangKe 2021/11/18 16:38
 *
 *
 * 当前测试类以及当前项目中的拦截器,过滤器参考:https://blog.51cto.com/zero01/2334836
 *
 *
 **/
@RestController
@RequestMapping("/user")
public class DemoController {

    //localhost:8080/user/register
    @PostMapping("/register")
    public UserParam register(@RequestBody UserParam userParam) {
        return userParam;
    }
}