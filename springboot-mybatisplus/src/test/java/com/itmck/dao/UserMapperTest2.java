package com.itmck.dao;

import com.alibaba.fastjson.JSON;
import com.itmck.page.PageUtil;
import com.itmck.page.PageVo;
import com.itmck.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/15 16:21
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest2 {


    @Test
    public void run() throws IOException {

        // 读取mybatis-config.xml文件
        InputStream inputStream = Resources.getResourceAsStream("mybatis/config/mybatis-config.xml");
        // 初始化mybatis，创建SqlSessionFactory类的实例
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        // 创建Session实例
        SqlSession session = sqlSessionFactory.openSession();
        // 获得mapper接口的代理对象
        UserMapper userMapper = session.getMapper(UserMapper.class);

        // 开启分页
        PageVo pageVo = new PageVo(0, 2);
        PageUtil.start(pageVo);
        List<User> sysUserLists = userMapper.selectUser();
        // 分页结束
         PageUtil.end();
        System.out.println("输出分页结果对象: " + JSON.toJSONString(pageVo));

        // 提交事务
        session.commit();
        // 关闭Session
        session.close();

    }

}