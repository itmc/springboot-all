package com.itmck.dao;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.itmck.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/15 16:21
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Resource
    private UserMapper userMapper;


    @Test
    public void insert() {

        User user = new User();
        user.setName("asd");
        user.setAge(30);
        user.setEmail("3474");
        userMapper.insert(user);
//        userMapper.insertUser(user);
    }

    @Test
    public void update() {

        User user = new User();
        user.setName("mcc");
        user.setAge(30);
        user.setEmail("3474");
        user.setCityCode("ewq");
        user.setId(1L);
        userMapper.updateUser(user);

//        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("id",1L);
//        userMapper.update(user,queryWrapper);
    }



    @Test
    public void list() {

        List<User> list = new LambdaQueryChainWrapper<>(userMapper)
                .list();
        System.out.println(list);
    }


    /**
     * 测试 LambdaQueryWrapper
     */
    @Test
    public void selectCount() {

        QueryWrapper<User> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("id","1");

        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .eq(User::getId, 1);
        Long aLong = userMapper.selectCount(queryWrapper);
        log.info("数量:{}", aLong);
    }


    /**
     * 测试 lambdaQueryChainWrapper
     */
    @Test
    public void lambdaQueryChainWrapper() {
        List<User> list = new LambdaQueryChainWrapper<>(userMapper)
                .eq(User::getName, "mck")
                .select(User::getEmail, User::getId)
                .list();
        log.info("list:{}", list);
    }


    /**
     * 测试 LambdaUpdateChainWrapper
     */
    @Test
//    @Transactional(propagation = Propagation.REQUIRED)
    public void LambdaUpdateChainWrapper() {
        boolean result = new LambdaUpdateChainWrapper<User>(userMapper)

                .eq(User::getId,1)
                .set(User::getName, "mmm")
                .set(User::getEmail,"347")
                .update();
//        ist();
//        int i = 1 / 0;
        log.info("list:{}", result);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void ist() {
        boolean result2 = new LambdaUpdateChainWrapper<>(userMapper)

                .set(User::getName, "mmm")
                .update();

    }

    /**
     * 测试自定义条件构造 elect * from user ${ew.customSqlSegment}
     */
    @Test
    public void selectByName() {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .eq(User::getName, "mck");
        User user = userMapper.selectByName(queryWrapper);
        log.info("user:{}", user);
    }
}