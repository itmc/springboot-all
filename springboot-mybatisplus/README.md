MyBatis-Plus 是 MyBatis 的第三方使用插件

> MyBatis-Plus（简称 MP）是一个 MyBatis 的增强工具，在 MyBatis 的基础上只做增强不做改变，为简化开发、提高效率而生。
>官网：https://mp.baomidou.com/

# 特性
官网说的特性太多了，挑了几个有特点的分享给大家。
- 无侵入
- 损耗小
- 强大的 CRUD 操作
- 支持 Lambda 形式调用
- 支持多种数据库
- 内置分页插件

# 目录结构
```text
├─src
│  ├─main
│  │  ├─java
│  │  │  └─com
│  │  │      └─itmck
│  │  │          ├─config       拦截器以及过滤器,Mp配置
│  │  │          ├─controller   控制器
│  │  │          ├─dao
│  │  │          ├─filter       自定义拦截器与过滤器
│  │  │          ├─pojo
│  │  │          ├─service
│  │  │          ├─util         自定义queryMapper工具类等
│  │  │          └─vo
│  │  └─resources
│  └─test
│      └─java
│          └─com
│              └─itmck
│                  └─dao
```

# sql
```sql
/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 80025
Source Host           : localhost:3306
Source Database       : spring_db

Target Server Type    : MYSQL
Target Server Version : 80025
File Encoding         : 65001

Date: 2021-11-17 14:53:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  `age` int DEFAULT NULL,
  `email` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'mck', '26', '1735580535');

```

# 依赖
```xml
 <dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.2.0</version>
</dependency>
```
# mp分页配置
```java
package com.itmck.filterconfig;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.itmck.dao")
public class MybatisPlusConfig {

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
```

# 测试
mapper
```java
@Repository
@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Select(value = "select * from user ${ew.customSqlSegment}")
    User selectByName(@Param("ew") Wrapper<User> updateWrapper);
}
```
测试
```java
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {

    @Resource
    private UserMapper userMapper;

    /**
     * 测试 LambdaQueryWrapper
     */
    @Test
    public void selectCount() {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .eq(User::getId, 1);
        Integer integer = userMapper.selectCount(queryWrapper);
        log.info("数量:{}", integer);
    }


    /**
     * 测试 lambdaQueryChainWrapper
     */
    @Test
    public void lambdaQueryChainWrapper() {
        List<User> list = new LambdaQueryChainWrapper<>(userMapper)
                .eq(User::getName, "mck")
                .select(User::getEmail, User::getId)
                .list();
        log.info("list:{}", list);
    }


    /**
     * 测试 LambdaUpdateChainWrapper
     */
    @Test
    public void LambdaUpdateChainWrapper() {
        boolean result = new LambdaUpdateChainWrapper<>(userMapper)
                .eq(User::getName, "mck")
                .set(User::getAge, 26).update();
        log.info("list:{}", result);
    }

    /**
     * 测试自定义条件构造 elect * from user ${ew.customSqlSegment}
     */
    @Test
    public void selectByName() {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<User>()
                .eq(User::getName, "mck");
        User user = userMapper.selectByName(queryWrapper);
        log.info("user:{}", user);
    }
}
```

# 实战项目案例

> 实际开发中可以进一步对mp进行封装,以满足前后端联调业务的对接.
>  这里封装了 MyQueryPageUtil工具类

## 封装com.itmck.util.MyQueryPageUtil
> 通过对QueryMapper的封装实现更简单的crud查询能力

```java
package com.itmck.util;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/8/20 15:07
 **/
public class MyQueryPageUtil {


    /**
     * @param request request请求域
     * @param mapper  基础mapper
     * @param <T>     泛型
     * @return 返回泛型类分页
     */
    public static <T> Page<T> builder(HttpServletRequest request, BaseMapper<T> mapper) {

        return builder(request, mapper, null);
    }

    /**
     * @param request      request请求域
     * @param mapper       基础mapper
     * @param queryWrapper 查询mapper
     * @param <T>          泛型
     * @return 返回泛型类分页
     */
    public static <T> Page<T> builder(HttpServletRequest request, BaseMapper<T> mapper, QueryWrapper<T> queryWrapper) {

        Long currentPage = 1L;
        Long pageSize = 10L;
        if (ObjectUtil.isNull(queryWrapper)) {
            queryWrapper = new QueryWrapper<>();
        }
        Map<String, Object> requestParamsMap = RequestUtil.getRequestJson(request);
        for (Map.Entry<String, Object> entry : requestParamsMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (StrUtil.isNotBlank(key)) {
                if (StrUtil.equals("pageSize", key)) {
                    pageSize = (Long) value;
                } else if (StrUtil.equals("currentPage", key)) {
                    currentPage = (Long) value;
                } else {
                    queryConditionCheck(mapper, queryWrapper, key, value);
                }
            }
        }
        return mapper.selectPage(new Page<>(currentPage, pageSize), queryWrapper);

    }

    /**
     * 获取数据集合
     *
     * @param request request请求域
     * @param mapper  基础mapper
     * @param <T>     泛型类
     * @return 返回泛型类集合
     */
    public static <T> List<T> builderList(HttpServletRequest request, BaseMapper<T> mapper) {

        return builderList(request, mapper, null);

    }

    /**
     * 获取数据集合
     *
     * @param request      request请求域
     * @param mapper       基础mapper
     * @param queryWrapper 查询mapper
     * @param <T>          泛型类
     * @return 返回泛型类集合
     */
    public static <T> List<T> builderList(HttpServletRequest request, BaseMapper<T> mapper, QueryWrapper<T> queryWrapper) {

        if (ObjectUtil.isNull(queryWrapper)) {
            queryWrapper = new QueryWrapper<>();
        }
        Map<String, Object> requestParamsMap = RequestUtil.getRequestParamsMap(request);
        for (Map.Entry<String, Object> entry : requestParamsMap.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (ObjectUtil.isNotNull(value))
                queryConditionCheck(mapper, queryWrapper, key, value);
        }
        return mapper.selectList(queryWrapper);
    }


    /**
     * 前端使用get请求传入如  ?eq$name=mck&age=26
     * 对应当前条件查询支持 eq,like,likeLeft,likeRight,ge,le 默认为eq
     *
     * @param mapper       基础mapper
     * @param queryWrapper 查询mapper
     * @param key          eq$name=mck&age=26 的 name,age
     * @param value        eq$name=mck&age=26 的 mck,26
     * @param <T>          泛型
     */
    private static <T> void queryConditionCheck(BaseMapper<T> mapper, QueryWrapper<T> queryWrapper, String key, Object value) {

        String[] split = key.split("\\$");
        List<String> entityFieldList = ReflectUtil.getEntityFieldList(mapper.getClass());
        if (entityFieldList.contains(split[0])) {
            return;
        }
        switch (split[0]) {
            case "in":
                queryWrapper.in(split[1], Arrays.asList(((String) value).split(",")));
                break;
            case "like":
                queryWrapper.like(split[1], value);
                break;
            case "likeLeft":
                queryWrapper.likeLeft(split[1], value);
                break;
            case "likeRight":
                queryWrapper.likeRight(split[1], value);
                break;
            case "ge":
                queryWrapper.ge(split[1], value);
                break;
            case "le":
                queryWrapper.le(split[1], value);
                break;
            case "eq":
            default:
                queryWrapper.eq(split[1], value);
                break;
        }
    }

}

```

## MyQueryPageUtil依赖ReflectUtil 实现表结构字段的校验

```java
package com.itmck.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/8/20 15:07
 **/
public class ReflectUtil {

    public static List<String> getEntityFieldList(Class<?> clazz) {

        ArrayList<Type> types = new ArrayList<>();
        ReflectUtil.getGenericTypes(types, clazz);
        try {
            Class<?> aClass = Class.forName(types.get(0).getTypeName());
            return Arrays.stream(aClass.getDeclaredFields())
                    .map(field -> {
                        if (field.isAnnotationPresent(TableId.class)) {
                            return field.getAnnotation(TableId.class).value();
                        }
                        if (field.isAnnotationPresent(TableField.class)) {
                            return field.getAnnotation(TableField.class).value();
                        }
                        return null;
                    })
                    .filter(StrUtil::isNotBlank)
                    .collect(Collectors.toList());

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();

    }

    private static void getGenericTypes(ArrayList<Type> types, Class<?> clazz) {

        Type genericSuperclass = clazz.getGenericSuperclass();
        if (genericSuperclass instanceof ParameterizedType) {
            Collections.addAll(types, ((ParameterizedType) genericSuperclass).getActualTypeArguments());
        }
        Type[] genericInterfaces = clazz.getGenericInterfaces();
        for (Type genericInterface : genericInterfaces) {
            if (genericInterface instanceof ParameterizedType) {
                Collections.addAll(types, ((ParameterizedType) genericInterface).getActualTypeArguments());
            }
        }

        Class<?> superclass = clazz.getSuperclass();
        if (ObjectUtil.isNotNull(superclass)) {
            getGenericTypes(types, superclass);
        }
        Class<?>[] interfaces = clazz.getInterfaces();
        for (Class<?> anInterface : interfaces) {
            getGenericTypes(types, anInterface);
        }

    }
}
```
## MyQueryPageUtil依赖RequestUtil 实现request中获取数据
```java
package com.itmck.util;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/8/20 14:16
 **/
@Slf4j
public class RequestUtil {

    public static String getRequestValueByKey(HttpServletRequest request, String key) {

        Map<String, Object> requestParamsMap = RequestUtil.getRequestParamsMap(request);
        return (String) Objects.requireNonNull(requestParamsMap.entrySet()
                .stream()
                .filter(e -> e.getKey().equals(key))
                .findFirst()
                .orElse(null))
                .getValue();
    }

    public static Map<String, Object> getRequestParamsMap(HttpServletRequest request) {

        HashMap<String, Object> emptyMap = new HashMap<>();
        Enumeration<String> parameterNames = request.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            String parameter = parameterNames.nextElement();
            if (StrUtil.isNotBlank(parameter)) {
                String value = request.getParameter(parameter);
                if (StrUtil.isNotBlank(parameter)) {
                    emptyMap.put(parameter, value);
                }
            }
        }
        return emptyMap;
    }


    public static Map<String, Object> getRequestMap(HttpServletRequest request) {
        String method = request.getMethod();
        if (StrUtil.equalsIgnoreCase("GET", method)) {
            return RequestUtil.getRequestParamsMap(request);
        }
        return RequestUtil.getRequestJson(request);
    }

    public static Map<String, Object> getRequestJson(HttpServletRequest request) {
        Map<String, Object> emptyMap = new HashMap<>();

        try (
                ServletInputStream inputStream = request.getInputStream()
        ) {
            String body = IoUtil.read(inputStream, Charset.defaultCharset());
            if (StrUtil.isNotBlank(body)) {
                emptyMap = JSON.parseObject(body, Map.class);
            }
        } catch (IOException e) {
           log.error("IO流关闭异常",e);
        }
        return emptyMap;
    }
}
```

## 测试
com.itmck.controller.MpController
```java
@Slf4j
@RestController
public class MpController {

    @Resource
    private UserService userService;

    @PostMapping("mpJson")//post请求
    @GetMapping("mpJson")//get请求
    public PageBodyResponse<UserDTO> loadPageList(HttpServletRequest request) {
        return userService.loadPageList(request);
    }
}
```
com.itmck.service.UserService
```java
public interface UserService {
    PageBodyResponse<UserDTO> loadPageList(HttpServletRequest request);
}
```
com.itmck.service.UserServiceImpl
```java

@Slf4j
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public PageBodyResponse<UserDTO> loadPageList(HttpServletRequest request) {
        Page<User> page = MyQueryPageUtil.builder(request, this.userMapper);
        return PageUtil.covertRecordType(page, UserDTO.class);
    }
}

```
Post请求 http://localhost:8080/mpJson
参数
```json
{
	"eq$name": "mck"
}
```
结果:
```json
{
	"list": [
		{
			"id": 1,
			"name": "mck",
			"age": 26,
			"email": "1735580535"
		}
	],
	"total": 1,
	"message": null,
	"errorCode": "200",
	"success": true
}
```

Get请求 http://localhost:8080/mpJson?eq$name=mck
结果与上述一致