package com.itmck;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootShiro {
    public static void main(String[] args) {

        SpringApplication.run(SpringbootShiro.class,args);
    }
}
