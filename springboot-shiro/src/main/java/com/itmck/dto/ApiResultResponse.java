package com.itmck.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/3 10:17
 **/
@Data
@Builder
public class ApiResultResponse<T> implements Serializable {

    /**
     * 错误信息
     */
    private String message;

    /**
     * 错误码
     */
    public String errorCode;

    /**
     * 是否成功
     */
    private boolean success;

    /**
     * 响应体
     */
    private T result;

    public ApiResultResponse() {
    }

    public ApiResultResponse(String message, String errorCode, boolean success, T result) {
        this.message = message;
        this.errorCode = errorCode;
        this.success = success;
        this.result = result;
    }

    public static <T> ApiResultResponse<T> ok(T data) {
        return new ApiResultResponse<>("ok", "200", true, data);
    }

    public static <T> ApiResultResponse<T> ok(String message, T data) {
        return new ApiResultResponse<>(message, "200", true, data);
    }

    public static <T> ApiResultResponse<T> error(String message, String errorCode) {
        return new ApiResultResponse<>(message, errorCode, false, null);
    }
}