package com.itmck.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 爱意随风起，风止意难平  ---网抑云
 * <p>
 * Create by M ChangKe  10:24
 * <p>
 * 页面跳转相关代码
 */
@Controller
@Slf4j
@RequestMapping("/")
public class ViewController {

    @RequestMapping("login")
    public String hello() {
        log.info("跳转到登录页面");
        return "login";
    }

    @RequestMapping("unAuth")
    public String unAuth(Model model) {
        log.info("跳转到未认证页面");
        model.addAttribute("msg","请先登录");
        return "unAuth";
    }

    @RequestMapping("unAuthor")
    public String unAuthor(Model model) {
        log.info("跳转到未授权页面");
        model.addAttribute("msg","没有权限");
        return "unAuthor";
    }

    @GetMapping("/index")
    public String gen() {
        log.info("跳转到首页");
        return "index";
    }

    /**
     * 注解方式使用权限
     * @return
     */
//    @RequiresPermissions(value = {"admin","111"},logical = Logical.OR)
    @RequestMapping("add")
    public String add() {
        log.info("跳转到新增页面");
        return "add";
    }

    @RequestMapping("edit")
    public String edit() {
        log.info("跳转到编辑页面");
        return "edit";
    }

}
