package com.itmck.config;

import com.itmck.contant.CommonConstants;
import lombok.Data;
import org.apache.shiro.authc.UsernamePasswordToken;


/**
 * 爱意随风起，风止意难平  ---网抑云
 * <p>
 * Create by M ChangKe  14:59
 * <p>
 * <p>
 * 自定义CustomToken extends UsernamePasswordToken 实现特殊参数传递
 */
@Data
public class CustomToken extends UsernamePasswordToken {

    /**
     * 登录类型
     */
    private String loginType;
    /**
     * 验证码
     */
    private long verificationCodeCode;

    public CustomToken(String username, String password) {
        super(username, password);
        this.setLoginType(CommonConstants.LOGIN_BY_USERNAME);
    }

    /**
     * @param username
     * @param verificationCodeCode
     * @Author liumeng5
     */
    public CustomToken(String username, long verificationCodeCode) {
        this.setUsername(username);
        this.setVerificationCodeCode(verificationCodeCode);
        this.setLoginType(CommonConstants.LOGIN_VERIFICATION_CODE);
    }

}


