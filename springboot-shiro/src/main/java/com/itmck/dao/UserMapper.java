package com.itmck.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.pojo.User;
import com.itmck.pojo.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper extends BaseMapper<User> {


    UserVO getRolesAndPermissionsByUserName(@Param("userName") String userName);
}
