package com.itmck.service;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.itmck.dao.UserMapper;
import com.itmck.pojo.User;
import com.itmck.pojo.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class UserServiceImpl implements UserService {


    @Resource
    private UserMapper userMapper;

    @Override
    public User getUserByUserName(String userName) {

        return new LambdaQueryChainWrapper<>(userMapper)
                .eq(User::getUserName, userName)
                .one();
    }

    @Override
    public Map<String, Set<String>> getRolesAndPermissionsByUserName(String userName) {

        HashMap<String, Set<String>> mp = new HashMap<>();
        UserVO userVO = userMapper.getRolesAndPermissionsByUserName(userName);
        if (null != userVO) {
            String permissions = userVO.getPermissions();
            String roleNames = userVO.getRoleNames();
            String roleIds = userVO.getRoleIds();
            mp.put("permissions", change(permissions));
            mp.put("roleNames", change(roleNames));
            mp.put("roleIds", change(roleIds));
        }
        return mp;
    }

    public Set<String> change(String str) {

        if (StrUtil.isBlank(str))
            return null;
        String[] split = str.split(",");
        return Arrays.stream(split)
                .collect(Collectors.toSet());
    }
}
