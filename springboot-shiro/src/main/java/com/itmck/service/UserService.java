package com.itmck.service;

import com.itmck.pojo.User;

import java.util.Map;
import java.util.Set;

public interface UserService {
    User getUserByUserName(String userName);

    Map<String, Set<String>> getRolesAndPermissionsByUserName(String userName);
}
