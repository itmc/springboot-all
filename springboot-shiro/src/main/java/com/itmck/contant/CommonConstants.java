package com.itmck.contant;

public class CommonConstants {

    /**
     * 用户登录方式 ： 密码登录  /   验证码登录
      */
    public static String LOGIN_BY_USERNAME = "LOGIN_BY_USERNAME";
    public static String LOGIN_VERIFICATION_CODE = "VERIFICATION_CODE";
}
