package com.itmck.pojo;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("lc_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userName;
    private String password;
}
