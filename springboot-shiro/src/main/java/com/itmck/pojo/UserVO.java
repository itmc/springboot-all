package com.itmck.pojo;


import lombok.Data;

@Data
public class UserVO {

    private String userName;

    private String password;

    private String roleIds;

    private String roleNames;

    private String permissions;
}
