# springboot常用手脚架集成
为了方便Springboot的学习,这里列举与springboot集成的常用框架的学习demo.
> 版本 Springboot 2.2.6.RELEASE

# 集成案例
|  项目   | 说明  |参考链接  |
|  ----  | ----  |----  |
| springboot-basic  | springboot基础 |http://www.ityouknow.com/spring-boot.html|
| springboot-AdminServer  | spring-boot-admin-starter-server2.2.4 ||
| springboot-client  | spring-boot-admin-starter-client 2.2.4 ||
| springboot-forest  | 极简http框架Forest |https://forest.dtflyx.com/|
| springboot-LiteFlow  | 流程引擎LiteFlow |https://liteflow.yomahub.com/|
| springboot-mybatisplus | 整合mybatis-plus |https://mp.baomidou.com/|
| springboot-SaToken  | 轻量级权限框架sa-token |https://sa-token.dev33.cn/|
| springboot-xxl-job | 整合xxl-job |https://www.xuxueli.com/xxl-job/|
| springboot-dydamic-datasources  | 自定义动态数据源 ||
| springboot-multipart-datasources | 分包方式多数据源 ||
| springboot-patterns  | 简单设计模式 ||
| springboot-rabbitmq  | 整合rabbitmq ||
| springboot-redis  | 整合redis ||
| springboot-SpringSecurity | SpringSecurity ||
| springboot-swagger | swagger基础 ||
| springboot-webservice | 整合cxf服务端 ||
| springboot-webservice-client | 整合cxf客户端 ||
