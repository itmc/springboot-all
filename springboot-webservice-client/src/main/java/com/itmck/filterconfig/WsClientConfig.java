package com.itmck.filterconfig;

import com.itmck.client.RiskIcrWsService;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WsClientConfig {

    @Value("${webservice.icrUrl}")
    private String icrUrl;

    @Bean("riskIcrWsServiceClient")
    public RiskIcrWsService riskIcrWsServiceClient() {
        JaxWsProxyFactoryBean jaxWsProxyFactoryBean = new JaxWsProxyFactoryBean();
        // 设置代理地址
        jaxWsProxyFactoryBean.setAddress(icrUrl);
        // 设置接口类型
        jaxWsProxyFactoryBean.setServiceClass(RiskIcrWsService.class);
        // 创建一个代理接口实现
        return jaxWsProxyFactoryBean.create(RiskIcrWsService.class);
    }
}