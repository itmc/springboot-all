
package com.itmck.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.itmck.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetRiskIcrResponse_QNAME = new QName("http://service.itmck.com/", "getRiskIcrResponse");
    private final static QName _GetRiskIcr_QNAME = new QName("http://service.itmck.com/", "getRiskIcr");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.itmck.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetRiskIcr }
     * 
     */
    public GetRiskIcr createGetRiskIcr() {
        return new GetRiskIcr();
    }

    /**
     * Create an instance of {@link GetRiskIcrResponse }
     * 
     */
    public GetRiskIcrResponse createGetRiskIcrResponse() {
        return new GetRiskIcrResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRiskIcrResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.itmck.com/", name = "getRiskIcrResponse")
    public JAXBElement<GetRiskIcrResponse> createGetRiskIcrResponse(GetRiskIcrResponse value) {
        return new JAXBElement<GetRiskIcrResponse>(_GetRiskIcrResponse_QNAME, GetRiskIcrResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRiskIcr }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.itmck.com/", name = "getRiskIcr")
    public JAXBElement<GetRiskIcr> createGetRiskIcr(GetRiskIcr value) {
        return new JAXBElement<GetRiskIcr>(_GetRiskIcr_QNAME, GetRiskIcr.class, null, value);
    }

}
