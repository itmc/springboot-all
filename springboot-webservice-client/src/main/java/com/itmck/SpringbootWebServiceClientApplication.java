package com.itmck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 17:11
 **/
@SpringBootApplication
public class SpringbootWebServiceClientApplication {


    public static void main(String[] args) {

        SpringApplication.run(SpringbootWebServiceClientApplication.class, args);
    }
}