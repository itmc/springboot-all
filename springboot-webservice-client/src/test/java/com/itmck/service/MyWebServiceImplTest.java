package com.itmck.service;

import com.itmck.client.RiskIcrWsService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 20:59
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyWebServiceImplTest {

    @Resource
    private RiskIcrWsService riskIcrWsService;

    @Test
    public void getRiskIcr() {

        String result = riskIcrWsService.getRiskIcr("mck");
        log.info("result:{}",result);
    }
}