package com.itmck.filterconfig;

import com.itmck.service.RiskIcrWsService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import javax.xml.ws.Endpoint;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 17:16
 **/
@Configuration
public class WsConfig {

    @Resource
    private RiskIcrWsService riskIcrWsService;
    @Resource
    private Bus bus;

    /**
     * 此方法作用是改变项目中服务名的前缀名，此处127.0.0.1或者localhost不能访问时，请使用ipconfig查看本机ip来访问
     * 此方法被注释后:wsdl访问地址为http://127.0.0.1:8080/services/icr?wsdl
     * 去掉注释后：wsdl访问地址为：http://127.0.0.1:8080/hzbank/icr?wsdl
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean<CXFServlet> cxfDispatcherServlet() {
        return new ServletRegistrationBean<>(new CXFServlet(), "/hzbank/*");
    }

    /**
     * JAX-WS
     * 站点服务
     **/
    @Bean(name = "endpointRiskIcrWsService")
    public Endpoint endpointRiskIcrWsService() {
        EndpointImpl endpoint = new EndpointImpl(bus, riskIcrWsService);
        endpoint.publish("/icr");
        return endpoint;
    }

}