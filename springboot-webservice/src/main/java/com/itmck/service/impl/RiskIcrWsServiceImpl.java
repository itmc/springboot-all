package com.itmck.service.impl;

import com.itmck.service.RiskIcrWsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.jws.WebService;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 17:13
 **/
@Slf4j
@WebService(serviceName = "RiskIcrWsService",//对外发布的服务名
        targetNamespace = "http://impl.service.itmck.com/",//指定你想要的名称空间，通常使用使用包名反转
        endpointInterface = "com.itmck.service.RiskIcrWsService")
@Component
public class RiskIcrWsServiceImpl implements RiskIcrWsService {
    
    @Override
    public String getRiskIcr(String msg) {
        log.info("请求参数:{}", msg);
        return "hello:" + msg;
    }
}