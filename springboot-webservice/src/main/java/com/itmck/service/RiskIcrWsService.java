package com.itmck.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * 太阳当空照,花儿对我笑
 * Create by M ChangKe 2021/11/22 17:21
 * 创建webservice服务接口
 **/
@WebService
public interface RiskIcrWsService {

    @WebMethod
    String getRiskIcr(@WebParam(name = "msg") String msg);
}
