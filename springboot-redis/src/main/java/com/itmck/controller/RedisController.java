package com.itmck.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/16 19:54
 **/
@RestController
public class RedisController {


    @GetMapping("/hello")
    public String hello() {
        return "hello redis";
    }
}