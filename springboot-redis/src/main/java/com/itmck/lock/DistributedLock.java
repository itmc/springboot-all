package com.itmck.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * 分布式锁接口
 * 在Lock接口的基础上增加了锁的失效时间
 *
 */
public interface DistributedLock extends Lock {
    /**
     * 不限时间的尝试加锁
     *
     * @param leaseTime 锁的失效时间
     * @param unit      时间单位
     */
    void lock(long leaseTime, TimeUnit unit);

    /**
     * 尝试加锁，超过等待时间时返回false
     *
     * @param waitTime  等待时间
     * @param leaseTime 锁的失效时间
     * @param unit      时间单位
     * @return true/false
     */
    boolean tryLock(long waitTime, long leaseTime, TimeUnit unit);
}