package com.itmck.controller;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RedisControllerTest {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    
    /**
     * 测试String
     */
    @Test
    public void testString() throws InterruptedException {

        ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
        valueOperations.set("itName", "mck");//设置
        String itName = valueOperations.get("itName");//获取
        log.info("itName:{}", itName);
//        redisTemplate.delete("itName");//删除
//        String itName2 = valueOperations.get("itName");
//        log.info("itName2:{}", itName2);


//        //如果key不存在设置key=value返回true,否则返回false
//        Boolean aBoolean = valueOperations.setIfAbsent("itName", "mck");
//        log.info("aBoolean:{}",aBoolean);
//
//
//        //如果key存在设置key=value返回true，否则返回false
//        Boolean bBoolean = valueOperations.setIfPresent("itName", "wxp");
//        log.info("aBoolean:{}",bBoolean);
//
//
//        valueOperations.set("itName", "wxp", 10000, TimeUnit.MILLISECONDS);
//        String itName2 = valueOperations.get("itName");//获取
//        log.info("itName:{}", itName2);
//        Thread.sleep(10000);
//        String itName3 = valueOperations.get("itName");//获取
//        log.info("itName:{}", itName3);

    }


    /**
     * 测试Hash类型
     */
    @Test
    public void testHash() {
        HashOperations<String, Object, Object> hashOperations = redisTemplate.opsForHash();
        hashOperations.put("user", "name", "tony");
        hashOperations.put("user", "age", "18");
        String name = (String) hashOperations.get("user", "name");
        String age = (String) hashOperations.get("user", "age");
        System.out.println("name = " + name);
        System.out.println("age = " + age);


    }

    /**
     * 测试List类型
     */
    @Test
    public void testList() {
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        listOperations.leftPush("language", "汉语");
        listOperations.rightPush("language", "英语");
        listOperations.leftPush("language", "法语");
        listOperations.leftPush("language", "德语");
        listOperations.leftPush("language", "西班牙语", "意大利语");
        List<String> language = listOperations.range("language", 0, 10);
        System.out.println("language = " + language);


    }

    /**
     * 测试Set类型
     */
    @Test
    public void testSet() {
        SetOperations<String, String> setOperations = redisTemplate.opsForSet();
        setOperations.add("numbers", "1", "2", "3", "5", "4");
        Set<String> numbers = setOperations.members("numbers");
        System.out.println("numbers = " + numbers);

    }

    /**
     * 测试Zset类型
     */
    @Test
    public void testZset() {
        ZSetOperations<String, String> zSetOperations = redisTemplate.opsForZSet();
        zSetOperations.add("myZset", "Spring", 5);
        zSetOperations.add("myZset", "Springmvc", 51);
        zSetOperations.add("myZset", "Springboot", 1);
        zSetOperations.add("myZset", "Springcloud", 3);
        Set<String> myZset = zSetOperations.range("myZset", 0, 10);
        System.out.println("myZset = " + myZset);
    }
}