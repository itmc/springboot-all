package com.itmck.filterconfig;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * 与SpringBoot集成, 保证此类被扫描，即可完成sa-token与SpringBoot的集成 
 * @author kong
 *
 */
@Component
public class SpringSaToken {

	
	/**
	 * 获取配置Bean 
	 * @return
	 */
	@Bean
	@ConfigurationProperties(prefix="mck")
	public SaTokenConfig getSaTokenConfig() {
		return new SaTokenConfig();
	}

	
	
}
