package com.itmck.filterconfig;

import lombok.Data;

/**
 * sa-token 总配置类  
 * @author kong
 *
 */
@Data
public class SaTokenConfig {

	private String tokenName = "satoken";		// token名称 (同时也是cookie名称)
	private long timeout = 30 * 24 * 60 * 60;	// token有效期，单位s 默认30天
	private Boolean isShare = true;			// 在多人登录同一账号时，是否共享会话 (为true时共用一个，为false时新登录挤掉旧登录)
	private Boolean isReadBody = true;			// 是否尝试从请求体里读取token
	private Boolean isReadHead = true;			// 是否尝试从header里读取token
	private Boolean isReadCookie = true;		// 是否尝试从cookie里读取token
	
	private Boolean isV = true;	 // 是否在初始化配置时打印版本字符画

}
