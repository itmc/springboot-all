# 自定义spring-boot-starter机制

---

## 为什么还要自定义starter

SpringBoot 最强大的功能就是把我们常用的场景抽取成了一个个starter（场景启动器），我们通过引入springboot 为
我提供的这些场景启动器，我们再进行少量的配置就能使用相应的功能。即使是这样,springboot也不能囊括我们所有
的使用场景，往往我们需要自定义starter，来简化我们对springboot的使用。

## 命名规范

官方建议tips:
- 官方的 starter 的命名格式为 `spring-boot-starter-{xxxx}` 如:spring-boot-starter-web
- 第三方我们自己的命名格式为 `{xxxx}-spring-boot-starter` 如:mybatis-spring-boot-starter

## 如何自定义

完整的springboot starter包含两个组件

- autoconfigure 这里包含自动配置的代码
- starter 依赖，或者依赖的其他springboot starter组件

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-autoconfigure</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-configuration-processor</artifactId>
        <optional>true</optional>
    </dependency>
</dependencies>
```

## 打包插件
```xml

<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <configuration>
                <encoding>${project.build.sourceEncoding}</encoding>
                <source>${java.version}</source>
                <target>${java.version}</target>
                <showWarnings>true</showWarnings>
            </configuration>
        </plugin>
    </plugins>
</build>
```

## 自动配置

### 方式1:spring.factories
创建resources\META-INF\spring.factories 
> org.springframework.boot.autoconfigure.EnableAutoConfiguration=com.itmck.mckspringbootstarter.config.DesAopConfig

### 方式2:@Configuration
```java
@Configuration
public class AppConfig {
    @Bean
    public Foo foo() {
        return new Foo();
    }
}
```

### 方式3.@Import 
@Import是 Spring Framework 中的注解之一，它可以用于在配置类中引入其他的配置类或者 Bean。

```
@Configuration
@Import({MyConfig1.class, MyConfig2.class})
public class AppConfig {
    // ...
}
```

在上面的例子中，@Import 注解被用于导入另外两个配置类 MyConfig1 和 MyConfig2，这样在 AppConfig 中就可以使用这两个配置类声明的
Bean 了。

### 方式4.导入 ImportSelector


```
public class MyImportSelector implements ImportSelector {
    @Override
    public String[] selectImports(AnnotationMetadata annotationMetadata) {
        return new String[]{DesAopConfig.class.getName()};
    }
}
```

```java
@Configuration
@Import(MyImportSelector.class)
public class AppConfig {
    // ...
}
```

在上面的例子中，@Import 注解被用于导入一个实现了 ImportSelector 接口的类 MyImportSelector，
通过 ImportSelector 接口和 @Import 注解可以实现自定义的 Bean 导入逻辑。

总的来说，@Import 注解是一个非常灵活的注解，可以用于导入各种类型的配置类、Bean 和 ImportSelector 实现类。

