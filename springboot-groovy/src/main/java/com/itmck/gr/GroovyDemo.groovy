package com.itmck.gr


/**
 * groovy学习
 *
 *
 */
class GroovyDemo {

    static void main(String[] args) {

//        /**
//         * 方法eachLine内置在Groovy中的File类中，目的是确保文本文件的每一行都被读取。
//         */
//        new File("E:/Example.txt").eachLine {
//            line -> println "line : $line";
//        }
//
//        /**
//         * 如果要将文件的整个内容作为字符串获取，可以使用文件类的text属性。
//         */
//        File file = new File("E:/Example.txt")
//
//        if (file.exists()) {
//            println file.text
//        }
//
//
//        /**
//         * 写入文件
//         */
//        new File('E:/', 'Example.txt').withWriter('utf-8') {
//            writer -> writer.writeLine 'Hello World'
//        }
//
//        /**
//         *
//         * 追加写入文件
//         */
//        new File('E:/', 'Example.txt').withWriterAppend("utf-8") {
//            writer -> writer.writeLine 'Hello World'
//        }
//
//
//        /**
//         *
//         * 获取文件大小
//         *
//         */
//        File fl = new File("E:/Example.txt")
//        println "The file ${fl.absolutePath} has ${fl.length()} bytes"
//
//
//        /**
//         * 测试文件是否是目录
//         */
//        def file = new File('E:/')
//        println "File? ${file.isFile()}"
//        println "Directory? ${file.isDirectory()}"
//
//
//        /**
//         *
//         * 复制文件
//         *
//         */
//
//        def src = new File("E:/Example.txt")
//        def dst = new File("E:/Example1.txt")
//        dst << src.text
//        print("文件复制成功")
//
//
//        /**
//         *
//         *
//         * 获取目录内容
//         */
//        def rootFiles = new File("E:/home").listRoots()
//        rootFiles.each {
//            ff -> println ff.absolutePath
//        }
//
//
//        /**
//         * 获取目录文件
//         */
//        new File("E:/home").eachFile() {
//            fs->println fs.getAbsolutePath()
//        }


        /**
         * 如果要递归显示目录及其子目录中的所有文件，
         */
//        new File("E:/home").eachFileRecurse() {
//            fs -> println fs.getAbsolutePath()
//        }
    }

}
