package com.itmck.gr

import groovy.text.XmlTemplateEngine

class TempUtil {

    static def text = '''
       <document xmlns:gsp='http://groovy.codehaus.org/2005/gsp'>
          <Student>
             <name>${StudentName}</name>
             <ID>${id}</ID>
             <subject>${subject}</subject>
          </Student>
       </document> 
    '''

    static String rest(def binding) {
        def engine = new XmlTemplateEngine()
        def template = engine.createTemplate(text).make(binding)
        return template.toString()
    }


}
