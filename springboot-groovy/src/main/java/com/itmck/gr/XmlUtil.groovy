package com.itmck.gr

import groovy.xml.MarkupBuilder

class XmlUtil {

    static def createXml() {

        def sw = new StringWriter()
        def x = new MarkupBuilder(sw)

        x.root('isRoot': true) {  // 括号里面是键值对就是属性，如果是单个值的话，就是中间的value值
            note() { // 如果这个标签下有子标签，就用闭包的形式来声明
                to('George')
                from('John')
                heading('Reminder')
                body("Don't forget the meeting!")
            }
            note() {
                to('George2')
                from('John2')
                heading('Reminder2')
                body("Don't forget the meeting!2")
            }
        }
        return sw
    }
}
