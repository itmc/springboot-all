package com.itmck.grv
//def s = """
//<!--   Copyright w3school.com.cn  -->
//<root>
//    <note>
//        <to>George</to>
//        <from>John</from>
//        <heading>Reminder</heading>
//        <body>Don't forget the meeting!</body>
//    </note>
//    <note>
//        <to>George2</to>
//        <from>John2</from>
//        <heading>Reminder2</heading>
//        <body>Don't forget the meeting2!</body>
//    </note>
//</root>
//"""
//
//def xml = new XmlSlurper().parseText(s)
//def list = xml.depthFirst().findAll { note ->
//    return note.from.text() == 'John' //这里从note开始点,查找出来的就是note
//}.collect {
//    it.to.text() //通过note获取下面的to节点的text
//}
//return list



//def sw = new StringWriter()
//def x = new MarkupBuilder(sw)
//
//x.root('isRoot':true) {  // 括号里面是键值对就是属性，如果是单个值的话，就是中间的value值
//    note () { // 如果这个标签下有子标签，就用闭包的形式来声明
//        to('George')
//        from('John')
//        heading('Reminder')
//        body("Don't forget the meeting!")
//    }
//    note () {
//        to('George2')
//        from('John2')
//        heading('Reminder2')
//        body("Don't forget the meeting!2")
//    }
//}
//return sw



//def name = "Groovy"
//
//def ss="""
//package com.itmck.java;
//import com.itmck.grv.GroovyXml;
//public class ${name} {
//    public static void main(String[] args) {
//        String s = new GroovyXml().run().toString();
//        System.out.println(s);
//
//    }
//}
//"""
////return ss


//def text ='This Tutorial focuses on $TutorialName. In this tutorial you will learn about $Topic'
//
//def binding = ["TutorialName":"Groovy", "Topic":"Templates"]
//def engine = new SimpleTemplateEngine()
//def template = engine.createTemplate(text).make(binding)
//
//return template


int[] arr= [1,2,3,4]
for (int i = 0; i < arr.length; i++) {
    print(arr[i])
}
