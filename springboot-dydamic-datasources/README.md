# springboot中自定义注解实现动态数据源切换

> Springboot+mp+动态数据源 针对动态数据源的切换操作可以扩展成读写分离.举例:如果读写分离可以使用中间件mycat.
> 使用多数据源也可以完成的.这里简单自定义注解实现多数据源切换.

# 目录结构
```text
├─src
│  ├─main
│  │  ├─java
│  │  │  └─com
│  │  │      └─itmck
│  │  │          ├─annotation  自定义数据源切换注解
│  │  │          ├─config      切面以及配置类
│  │  │          ├─dao
│  │  │          ├─entity
│  │  │          ├─enums
│  │  │          ├─handler
│  │  │          └─service
│  │  └─resources
│  └─test
│      └─java
│          └─com
│              └─itmck
│                  └─service

```

# 项目版本
> springboot 2.2.6.RELEASE+ Mybatis-Plus 3.2.0

# 引入依赖
```text
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.2.0</version>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```
# application.yml配置
> 注意: url改成 jdbc-url 否则报错: jdbcUrl is required with driverClassName
> 原文链接：https://blog.csdn.net/newbie_907486852/article/details/81391525
```yaml
server:
  port: 8080

spring:
  datasource:
    master:
      #这里url必须改为:jdbc-url 用来重写自定义连接池
      jdbc-url: jdbc:mysql://localhost:3306/spring_db?characterEncoding=utf-8&useSSL=false&serverTimezone=GMT%2B8
      driver-class-name: com.mysql.cj.jdbc.Driver
      username: root
      password: 123456
    slave:
      jdbc-url: jdbc:mysql://localhost:3306/spring_db2?characterEncoding=utf-8&useSSL=false&serverTimezone=GMT%2B8
      driver-class-name: com.mysql.cj.jdbc.Driver
      username: root
      password: 123456
    type: com.zaxxer.hikari.HikariDataSource
    hikari:
      connection-timeout: 30000
      idle-timeout: 30000
      auto-commit: 'true'
      minimum-idle: 5
      maximum-pool-size: 15
      pool-name: HikariCP
      connection-test-query: SELECT 1 FROM DUAL
      max-lifetime: 1800000

mybatis-plus:
  mapper-locations: 'classpath*:mapper/*/*Mapper.xml'
  type-aliases-package: com.itmck.entity
  configuration:
    log-impl: org.apache.ibatis.logging.stdout.StdOutImpl



```


# 自定义注解

```java
package com.itmck.annotation;

import enums.com.itmck.DataSourceType;

import java.lang.annotation.*;

/**
 * 自定义多数据源切换注解
 *
 * @author miaochangke
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Ds {
    /**
     * 切换数据源名称
     */
    public DataSourceType value() default DataSourceType.MASTER;
}
```

# 创建枚举
```java
package com.itmck.enums;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 14:28
 * <p>
 * 定义数据源枚举
 **/
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
```

# 自定义DynamicDataSourceContextHolder
> 用来管理线程变量,根据线程进行数据源切换
```java
package com.itmck.handler;

import lombok.extern.slf4j.Slf4j;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 14:31
 **/
@Slf4j
public class DynamicDataSourceContextHolder {

    /**
     * 使用ThreadLocal维护变量，ThreadLocal为每个使用该变量的线程提供独立的变量副本，
     * 所以每一个线程都可以独立地改变自己的副本，而不会影响其它线程所对应的副本。
     */
    private static final ThreadLocal<String> CONTEXT_HOLDER = new ThreadLocal<>();

    /**
     * 设置数据源的变量
     */
    public static void setDateSourceType(String dsType) {
        log.info("切换到{}数据源", dsType);
        CONTEXT_HOLDER.set(dsType);
    }

    /**
     * 获得数据源的变量
     */
    public static String getDateSourceType() {
        return CONTEXT_HOLDER.get();
    }

    /**
     * 清空数据源变量
     */
    public static void clearDateSourceType() {
        CONTEXT_HOLDER.remove();
    }
}
```

# 自定义DynamicDataSource
> 通过自定义DynamicDataSource extends AbstractRoutingDataSource 重写determineCurrentLookupKey()来选择当前线程对应数据源

```java
package com.itmck.handler;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.util.Map;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 14:37
 * <p>
 * 动态数据源
 **/
public class DynamicDataSource extends AbstractRoutingDataSource {


    public DynamicDataSource(DataSource defaultTargetDataSource, Map<Object, Object> targetDataSources) {
        super.setDefaultTargetDataSource(defaultTargetDataSource);//设置默认数据源
        super.setTargetDataSources(targetDataSources);//设置目标数据源
        super.afterPropertiesSet();
    }

    @Override
    protected Object determineCurrentLookupKey() {
        return DynamicDataSourceContextHolder.getDateSourceType();//选择数据源
    }
}

```

# 创建DynamicDataSourceConfig

```java
package com.itmck.filterconfig;

import enums.com.itmck.DataSourceType;
import handler.com.itmck.DynamicDataSource;
import com.zaxxer.hikari.HikariDataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 14:43
 **/
@Configuration
@MapperScan("com.itmck.dao")
public class DynamicDataSourceConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.master")
    public DataSource primaryDateSource() {
        return DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .build();
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.slave")
    public DataSource slaveDateSource() {
        return DataSourceBuilder.create()
                .type(HikariDataSource.class)
                .build();
    }


    @Bean(name = "dynamicDataSource")
    @Primary
    public DynamicDataSource dataSource() {
        Map<Object, Object> targetDataSources = new HashMap<>();
        targetDataSources.put(DataSourceType.MASTER.name(), primaryDateSource());
        targetDataSources.put(DataSourceType.SLAVE.name(), slaveDateSource());
        return new DynamicDataSource(primaryDateSource(), targetDataSources);
    }
}
```
# 创建切面
> 使用切面针对使用了注解@Ds的方法进行数据源的切换

```java
package com.itmck.filterconfig;

import annotation.com.itmck.Ds;
import handler.com.itmck.DynamicDataSourceContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/22 14:39
 **/
@Slf4j
@Aspect
@Order(1)
@Component
public class DataSourceAspect {


    @Pointcut("@annotation(com.itmck.annotation.Ds)")
    public void dsPointCut() {

    }

    @Around("dsPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        Ds ds = method.getAnnotation(Ds.class);
        if (null != ds) {
            DynamicDataSourceContextHolder.setDateSourceType(ds.value().name());
        }
        try {
            return point.proceed();
        } finally {
            // 销毁数据源 在执行方法之后
            DynamicDataSourceContextHolder.clearDateSourceType();
        }
    }
}
```
# 使用 
> 参考DsChangeImpl#getStudentList()  切换数据源: @Ds(value = DataSourceType.SLAVE)

```java
package com.itmck.service;

import com.baomidou.mybatisplus.extension.service.additional.query.impl.LambdaQueryChainWrapper;
import annotation.com.itmck.Ds;
import dao.com.itmck.StudentMapper;
import dao.com.itmck.UserMapper;
import entity.com.itmck.Student;
import entity.com.itmck.User;
import enums.com.itmck.DataSourceType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:49
 **/
@Service
public class DsChangeImpl implements DsChange {

    @Resource
    private UserMapper userMapper;

    @Resource
    private StudentMapper studentMapper;

    //使用默认数据源
    @Override
    public List<User> getUserList() {
        return new LambdaQueryChainWrapper<>(userMapper).list();
    }

    //使用从数据源
    @Override
    @Ds(value = DataSourceType.SLAVE)
    public List<Student> getStudentList() {
        return new LambdaQueryChainWrapper<>(studentMapper).list();
    }
}
```
# 启动类
> @SpringBootApplication(exclude = {DataSourceAutoConfiguration.class}) 排除数据源自动配置类
```java
package com.itmck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:03
 **/
//@Import(DynamicDataSourceConfig.class)
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableTransactionManagement
public class SpringbootDynamicDataSourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDynamicDataSourcesApplication.class, args);
    }
}
```

# 测试类

```java
package com.itmck.service;

import entity.com.itmck.Student;
import entity.com.itmck.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:49
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class DsChangeImplTest {

    @Resource
    private DsChange dsChange;

    @Test
    public void userMapper() {
        List<User> list = dsChange.getUserList();
        log.info("list:{}", list);
    }

    @Test
    public void studentMapper() {
        List<Student> list = dsChange.getStudentList();
        log.info("list:{}", list);
    }
}
```

分别输出:
```text
==>  Preparing: SELECT id,name,email,age FROM user 
==> Parameters: 
<==    Columns: id, name, email, age
<==        Row: 1, mck, 1735580535, 26
<==      Total: 1
list:[User(id=1, name=mck, age=26, email=1735580535)]
```
```text
切换到SLAVE数据源
==>  Preparing: SELECT id,name,age FROM student 
==> Parameters: 
<==    Columns: id, name, age
<==        Row: 1, wxp, 25
<==      Total: 1
Closing non transactional SqlSession [org.apache.ibatis.session.defaults.DefaultSqlSession@51b01550]
list:[Student(id=1, name=wxp, age=25)]

```