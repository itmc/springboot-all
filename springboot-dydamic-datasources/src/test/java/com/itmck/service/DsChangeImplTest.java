package com.itmck.service;

import com.itmck.entity.Student;
import com.itmck.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:49
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class DsChangeImplTest {

    @Resource
    private DsChange dsChange;
    @Resource
    private DsChange2 dsChange2;

    @Test
    public void userMapper() {
        List<User> list = dsChange.getUserList();
        log.info("list:{}", list);
    }

    @Test
    public void updateUser() {
        User user = new User();
        user.setName("mck");
        user.setEmail("123");
        int result = dsChange.updateUser(user);
        log.info("result:{}", result);


        Student student = new Student();
        student.setName("wxp");
        student.setAge(11);

        int result2 = dsChange2.updateStudent(student);
        log.info("result2:{}",result2);
    }

    @Test
    public void studentMapper() {
        List<Student> list = dsChange.getStudentList();
        log.info("list:{}", list);
    }
}