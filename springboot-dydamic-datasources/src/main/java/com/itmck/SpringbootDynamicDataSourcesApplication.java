package com.itmck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:03
 **/
//@Import(DynamicDataSourceConfig.class)
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableTransactionManagement
public class SpringbootDynamicDataSourcesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootDynamicDataSourcesApplication.class, args);
    }
}