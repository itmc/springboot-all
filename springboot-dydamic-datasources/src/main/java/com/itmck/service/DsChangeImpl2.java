package com.itmck.service;

import com.baomidou.mybatisplus.extension.service.additional.update.impl.LambdaUpdateChainWrapper;
import com.itmck.annotation.Ds;
import com.itmck.dao.StudentMapper;
import com.itmck.entity.Student;
import com.itmck.enums.DataSourceType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 15:49
 **/
@Service
public class DsChangeImpl2 implements DsChange2 {

    @Resource
    private StudentMapper studentMapper;

    @Override
    @Ds(value = DataSourceType.SLAVE)
    @Transactional(rollbackFor = Exception.class)//事务
    public int updateStudent(Student student) {
        boolean update = new LambdaUpdateChainWrapper<>(this.studentMapper)
                .eq(Student::getName, student.getName())
                .update(student);
        int i = 1 / 0; //测试事务
        return update ? 1 : 0;
    }

}