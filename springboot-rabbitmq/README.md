springboot整合rabbitmq
> rabbitmq与springboot集成的使用案例

# 目录结构说明
```text
├─src
│  ├─main
│  │  ├─java
│  │  │  └─com
│  │  │      └─itmck
│  │  │          ├─config           列举各种类型的mq配置
│  │  │          ├─controller
│  │  │          ├─mq
│  │  │          │  ├─confirm       创建直连交换机+消息确认队列
│  │  │          │  ├─deadconfirm   直连+死信+消息确认
│  │  │          │  ├─directed      直连交换机
│  │  │          │  ├─fanout        扇形交换机
│  │  │          │  ├─queue         简单队列创建
│  │  │          │  └─topic         主题交换机
│  │  │          ├─pojo             实体
│  │  │          └─service
│  │  └─resources
│  └─test
│      └─java
│          └─com
│              └─itmck
│                  └─controller    测试类用例              
```

# mq必须依赖
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```

# 配置application.yml

```yaml
spring:
  rabbitmq:
    addresses: 81.69.8.183:5672
    virtual-host: /rabbitmq
    password: admin
    username: admin
    # correlated发布成功触发回调.默认none:禁用发布确认模式
    publisher-confirm-type: correlated
    #发送失败退回,路由队列触发回调
    publisher-returns: true
    listener:
      simple:
        acknowledge-mode: manual
```
# 创建config

```java
package com.itmck.filterconfig;

import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 13:50
 * <p>
 * <p>
 * 说明.引入Jackson2JsonMessageConverter转换器传输Json数据
 **/
@Configuration
public class RabbitTemplateConfig {

    @Bean
    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
        return new RabbitAdmin(connectionFactory);
    }

    /**
     * 这是生产端,使用RabbitTemplate连接
     *
     * @param connectionFactory 连接工厂
     * @return
     */
    @Bean
    @Scope("prototype")
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(new Jackson2JsonMessageConverter());
        return template;
    }


    /**
     * 这是配置在消费端,SimpleRabbitListenerContainerFactory进行连接
     *
     * @param connectionFactory 连接工厂
     * @return
     */
    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        return factory;
    }

}
```

![avatar](doc/1626537049112-3f25ed58-4e99-47ca-8500-18410a270094.png)
