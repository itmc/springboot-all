package com.itmck.controller;

import com.itmck.mq.confirm.SendConfirm;
import com.itmck.mq.deadconfirm.DeadSendConfirm;
import com.itmck.mq.topic.TopicSender;
import com.itmck.pojo.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/16 20:02
 **/

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class RabbitMqControllerTest {

    @Resource
    private RabbitTemplate rabbitTemplate;
    @Resource
    TopicSender topicSender;
    @Resource
    private SendConfirm sendConfirm;
    @Resource
    private DeadSendConfirm deadSendConfirm;

    /**
     * 简单队列测试
     */
    @Test
    public void hello() {
        for (int i = 0; i < 10; i++) {
            String message = "hello single queue";
            log.info("发送消息:{}", message);
            this.rabbitTemplate.convertAndSend("hello", message);
        }
    }

    /**
     * 直连交换机
     */
    @Test
    public void direct() {

        String message = "hello direct exchange queue";
        log.info("发送消息:{}", message);
        this.rabbitTemplate.convertAndSend("direct_Exchange", "direct_key", message);
    }

    /**
     * 扇形交换机测试
     */
    @Test
    public void fan() {

        Date date = new Date();
        String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        String message = "FanSender1111:" + dateString;
        this.rabbitTemplate.convertAndSend("fanoutExchange", "", message);

    }


    /**
     * top交换机
     */
    @Test
    public void topicSenderTest() {
        topicSender.topicSend1();
        topicSender.topicSend2();
        topicSender.topicSend3();
    }


    /**
     * 直连交换机+消息确认机制
     */
    @Test
    public void sendConfirm() {

        String message = "hello confirm exchange queue";
        log.info("发送消息:{}", message);
        sendConfirm.sendMessage("confirm_Exchange", "confirm_key", message);
    }


    /**
     * 直连交换机+消息确认机制+死信队列
     */
    @Test
    public void sendDeadConfirm() {

        String message = "hello dead confirm exchange queue";
        log.info("发送消息:{}", message);
        deadSendConfirm.sendMessage("spring.direct.exchange", "spring.routing", message);
    }

    /**
     * 通过配置类 com.itmck.config.RabbitTemplateConfig 进行配置是用JSON转换器将消息转为json传输
     *
     * 测试json
     */
    @Test
    public void json() {
        User user = new User();
        user.setName("mck");
        user.setAge(26);
        log.info("发送消息:{}", user);
        this.rabbitTemplate.convertAndSend("hello2", user);
    }

}