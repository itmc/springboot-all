package com.itmck.filterconfig;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/7/17 16:00
 **/
@Configuration
public class OneByOneConfig {


    /**
     * 简单模式就是使用简单的队列进行监听.一个生产者一个消费者
     * 工作队列方式:在工人之间分配任务（竞争消费者模式）,能者多劳动
     *
     * @return
     */
    @Bean
    public Queue queue() {
        /**
         * name:队列名
         * durable:是否持久化,默认false.持久化队列会被存储在磁盘上.当消息代理重启仍然存在
         * exclusive:默认false,只能被当前的连接使用.当前连接关闭后队列被删除,优先级高于durable
         * autoDelete:默认false.没有生产者或者消费者,队列会被删除
         * @return
         */
        // return new Queue("hello");//源码可知当前和下面一样功能
        return new Queue("hello", true, false, false);
    }

    @Bean
    public Queue queue2() {
        return new Queue("hello2");
    }
}