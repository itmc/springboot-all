package com.itmck.filterconfig;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 11:50
 *
 * 说明:直连交换机+死信队列+消息确认机制
 *
 *
 **/
@Configuration
public class DeadConfig {

    //创建队列
    @Bean
    public Queue ttlQueue() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-dead-letter-exchange", "spring.direct.deadExchange");
        args.put("x-dead-letter-routing-key", "spring.deadRouting");
        //args.put("x-message-ttl",10000);  // 设置消息过期时间无效果
        //args.put("x-expires",10000);  // 设置队列过期时间无效果
        return new Queue("spring.direct.ttl.queue", true, false, false, args);
    }

    //创建死信队列
    @Bean
    public Queue deadQueue() {
        return new Queue("spring.direct.deadQueue");
    }

    //创建交换机
    @Bean
    public DirectExchange deadDirectExchange() {
        return new DirectExchange("spring.direct.exchange");
    }

    // 这里直接将死信队列以及正常消费队列都绑定到一个交换机上
    @Bean
    public Binding queueBindingDead1() {
        return BindingBuilder.bind(ttlQueue()).to(deadDirectExchange()).with("spring.routing");
    }

    @Bean
    public Binding queueBindingDead2() {
        return BindingBuilder.bind(deadQueue()).to(deadDirectExchange()).with("spring.deadRouting");
    }
}