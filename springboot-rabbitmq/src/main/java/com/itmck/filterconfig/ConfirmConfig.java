package com.itmck.filterconfig;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 11:29
 *
 *
 * 说明:创建直连交换机与队列的绑定  这里结合直连交换机+消息确认机制使用
 *
 *
 **/
@Configuration
public class ConfirmConfig {

    @Bean
    public Queue confirmQueue() {
        return new Queue("confirm_queue");
    }


    /**
     * 直连交换机
     *
     * @return
     */
    @Bean
    public DirectExchange confirmExchange() {
        return new DirectExchange("confirm_Exchange");
    }


    /**
     * 绑定
     *
     * @param directQueue    队列
     * @param directExchange 交换机
     * @return
     */
    @Bean
    public Binding bindingConfirmDirectExchange(Queue directQueue, DirectExchange directExchange) {
        return BindingBuilder
                .bind(directQueue)
                .to(directExchange)
                .with("confirm_key");//路由键 direct_key
    }
}