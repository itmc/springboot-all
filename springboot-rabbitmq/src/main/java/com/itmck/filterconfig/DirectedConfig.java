package com.itmck.filterconfig;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/17 11:29
 **/
@Configuration
public class DirectedConfig {

    @Bean
    public Queue directQueue() {
        return new Queue("direct_queue");
    }


    /**
     * 直连交换机
     *
     * @return
     */
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("direct_Exchange");
    }


    /**
     * 绑定
     *
     * @param directQueue    队列
     * @param directExchange 交换机
     * @return
     */
    @Bean
    public Binding bindingDirectExchange(Queue directQueue, DirectExchange directExchange) {
        return BindingBuilder
                .bind(directQueue)
                .to(directExchange)
                .with("direct_key");//路由键 direct_key
    }
}