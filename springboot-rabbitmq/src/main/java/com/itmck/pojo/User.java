package com.itmck.pojo;

import lombok.Data;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/15 16:14
 **/
@Data
public class User {
    
    private Long id;
    private String name;
    private Integer age;
    private String email;


}