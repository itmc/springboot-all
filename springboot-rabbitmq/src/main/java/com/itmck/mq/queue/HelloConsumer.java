package com.itmck.mq.queue;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/7/17 16:00
 **/
@Slf4j
@Component
@RabbitListener(queues = "hello")
public class HelloConsumer {

    @RabbitHandler
    public void process(String message) {
        log.info("消费端接收消息: {}", message);
    }

}