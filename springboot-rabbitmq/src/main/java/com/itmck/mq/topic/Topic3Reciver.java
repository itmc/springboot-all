package com.itmck.mq.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "topic.c")
public class Topic3Reciver {

    @RabbitHandler
    public void receiver(String msg) {
        System.out.println("topic.C--Receiver::::" + msg);
    }
}