package com.itmck.mq.topic;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class TopicSender {


    @Resource
    private AmqpTemplate rabbitTemplate;

    public void topicSend1() {
        Date date = new Date();
        String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        dateString = "[topic.msg] Send1 msg:" + dateString;
        System.out.println(dateString);
        this.rabbitTemplate.convertAndSend("topicExchange", "topic.msg", dateString);
    }

    public void topicSend2() {
        Date date = new Date();
        String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        dateString = "[topic.good.msg] Send2 msg:" + dateString;
        System.out.println(dateString);
        this.rabbitTemplate.convertAndSend("topicExchange", "topic.good.msg", dateString);
    }

    public void topicSend3() {
        Date date = new Date();
        String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
        dateString = "[topic.m.z] Send3 msg:" + dateString;
        System.out.println(dateString);
        this.rabbitTemplate.convertAndSend("topicExchange", "topic.m.z", dateString);
    }
}