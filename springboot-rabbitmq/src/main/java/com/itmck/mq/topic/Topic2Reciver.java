package com.itmck.mq.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "topic.b")
public class Topic2Reciver {

    @RabbitHandler
    public void receiver(String msg) {
        System.out.println("topic.B--Receiver::::" + msg);
    }
}
