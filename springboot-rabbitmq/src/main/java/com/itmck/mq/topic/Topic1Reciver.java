package com.itmck.mq.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "topic.a")
public class Topic1Reciver {

    @RabbitHandler
    public void receiver(String message){
        System.out.println("topic.A--Receiver::::"+message);
    }
}
