package com.itmck.controller;

import com.itmck.mq.confirm.SendConfirm;
import com.itmck.mq.deadconfirm.DeadSendConfirm;
import com.itmck.service.RabbbitMqService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/16 20:01
 **/
@Slf4j
@RestController
public class RabbitMqController {

    @GetMapping("/mq")
    public String rabbitmq() {
        return "hello rabbit mq";
    }


    @Resource
    private RabbbitMqService rabbbitMqService;


    @ApiOperation(value = "创建死信队列及其交换机", notes = "创建消息队列以及交换机")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "queueName", value = "正常消费队列", required = true, dataType = "String", paramType = "path", example = "tansun_ind"),
            @ApiImplicitParam(name = "dlxQueueName", value = "死信队列", required = true, dataType = "String", paramType = "path", example = "tansun_dlx"),
            @ApiImplicitParam(name = "dlExchangeName", value = "死信交换机", required = true, dataType = "String", paramType = "path", example = "tansun_DL_Ex")
    })
    @GetMapping(value = "/ct/{queueName}/{dlxQueueName}/{dlExchangeName}")
    public String createDlxExchange(@PathVariable("queueName") String queueName,
                                    @PathVariable("dlxQueueName") String dlxQueueName,
                                    @PathVariable("dlExchangeName") String dlExchangeName) {


        //参数检查
        if (!dlExchangeName.contains("_DL_Ex")) {
            return "死信交换机以 _DL_Ex结尾";
        }
        if (!dlxQueueName.contains("_dlx")) {
            return "死信队列以 _dlx结尾";
        }
        return rabbbitMqService.createDlxExchange(queueName, dlxQueueName, dlExchangeName);
    }



    @Resource
    private SendConfirm sendConfirm;
    @Resource
    private DeadSendConfirm deadSendConfirm;

    @GetMapping(value = "/cm")
    public String confirmMethod(){
        String message = "hello confirm exchange queue";
        log.info("发送消息:{}", message);
        sendConfirm.sendMessage("confirm_Exchange", "confirm_key", message);
        return "success";
    }


    @GetMapping(value = "/dm")
    public String deadMethod(){
        String message = "hello dead confirm exchange queue";
        log.info("发送消息:{}", message);
        deadSendConfirm.sendMessage("spring.direct.exchange", "spring.routing", message);
        return "success";
    }
}