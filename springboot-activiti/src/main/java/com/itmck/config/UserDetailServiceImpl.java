package com.itmck.config;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 解决 act报错
 *
 * Parameter 0 of method userGroupManager in org.activiti.core.common.spring.identity.config.ActivitiSpringIdentityAutoConfiguration required a bean of type 'org.springframework.security.core.userdetails.UserDetailsService' that could not be found.
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return null;
    }
}

