package com.itmck;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * 简单工作流
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
class LeaveTest {


    @Resource
    private RepositoryService repositoryService;//activiti的资源管理类
    @Resource
    private RuntimeService runtimeService; //activiti的流程运行管理类
    @Resource
    private TaskService taskService;  //activiti的任务管理类
    @Resource
    private HistoryService historyService;  //activiti的历史管理类
    @Resource
    private ManagementService managementService; //activiti的引擎管理类

    /**
     * 启动流程实例
     */
    @Test
    public void testStartProcess() {
        // 1、发布流程
        Deployment deployment = repositoryService.createDeployment()
                .name("请假流程")//流程名
                .key("myLeave")//流程实例key
                .addClasspathResource("processes/Leave.bpmn")
                .deploy()
                ;
        // 2.根据流程定义Id启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("myLeave");
        // 输出内容
        log.info("流程定义id：{}", processInstance.getProcessDefinitionId());
        log.info("流程实例id：{}", processInstance.getId());
        log.info("当前活动Id：{}", processInstance.getActivityId());
    }

    /**
     * 查询当前个人待执行的任务
     */
    @Test
    public void testFindPersonalTaskList() {
        // 任务负责人 worker manager  boss
        String assignee = "worker";
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("myLeave") //流程Key
                .taskAssignee(assignee)//只查询该任务负责人的任务
                .list();
        for (Task task : list) {
            log.info("流程实例id：{}", task.getProcessInstanceId());
            log.info("任务id：{}", task.getId());
            log.info("任务负责人：{}", task.getAssignee());
            log.info("任务名称：{}", task.getName());
        }
    }

    // 完成任务
    @Test
    public void completTask() {
        // 根据流程key 和 任务的负责人 查询任务
        // 返回一个任务对象
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("myLeave") //流程Key
                .taskAssignee("worker") //要查询的负责人
                .singleResult();


        // 完成任务,参数：任务id
        taskService.complete(task.getId());
    }


    //根据流程实例获取当前活动任务
    @Test
    public  void queryTask(){
        String instanceId="9b6a5c11-f14c-11ec-a1c5-00ff7b7dfee4";
        Task  task = taskService.createTaskQuery()
                .processInstanceId(instanceId)
                .active()
                .singleResult();
        String taskId = task.getId();
        System.out.println("任务ID"+taskId);
    }

    /**
     * 查询流程定义
     */
    @Test
    public void queryProcessDefinition() {
        // 得到ProcessDefinitionQuery 对象
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        // 查询出当前所有的流程定义
        // 条件：processDefinitionKey =evection
        // orderByProcessDefinitionVersion 按照版本排序
        // desc倒叙
        // list 返回集合
        List<ProcessDefinition> definitionList =
                processDefinitionQuery.processDefinitionKey("myLeave")
                        .orderByProcessDefinitionVersion()
                        .desc()
                        .list();
        // 输出流程定义信息
        for (ProcessDefinition processDefinition : definitionList) {
            log.info("流程定义 id= {}" , processDefinition.getId());
            log.info("流程定义name= {}" , processDefinition.getName());
            log.info("流程定义 key= {}" ,processDefinition.getKey());
            log.info("流程定义Version= {}",processDefinition.getVersion());
            log.info("流程部署ID= {}", processDefinition.getDeploymentId());
        }
    }

    /**
     *        34b61eb4-f149-11ec-ad4f-00ff7b7dfee4
     *        328b285f-f149-11ec-ad4f-00ff7b7dfee4
     */
    @Test
    public void deleteDeployment() {
        // 流程部署id
        String deploymentId = "34b61eb4-f149-11ec-ad4f-00ff7b7dfee4";
        //删除流程定义，如果该流程定义已有流程实例启动则删除时出错
//        repositoryService.deleteDeployment(deploymentId);
//        //设置true 级联删除流程定义，即使该流程有流程实例启动也可以删除，设置为false非级连删除方式
        repositoryService.deleteDeployment(deploymentId, true);
    }

}
