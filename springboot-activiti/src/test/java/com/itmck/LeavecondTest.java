package com.itmck;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * 简单工作流
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
class LeavecondTest {


    @Resource
    private RepositoryService repositoryService;//activiti的资源管理类
    @Resource
    private RuntimeService runtimeService; //activiti的流程运行管理类
    @Resource
    private TaskService taskService;  //activiti的任务管理类
    @Resource
    private HistoryService historyService;  //activiti的历史管理类
    @Resource
    private ManagementService managementService; //activiti的引擎管理类

    /**
     * 启动流程实例
     *
     * 流程定义id：Leavecond:2:bb6536e3-f16b-11ec-8308-00ff7b7dfee4
     * 流程实例id：bb67a7e4-f16b-11ec-8308-00ff7b7dfee4
     * 当前活动Id：null
     */
    @Test
    public void testStartProcess() {
        // 1、发布流程
        Deployment deployment = repositoryService.createDeployment()
                .name("请假流程")//流程名
                .key("Leavecond")//流程实例key
                .addClasspathResource("processes/Leavecond.bpmn")
                .deploy()
                ;

        // 2.根据流程定义Id启动流程
        ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("Leavecond");
        // 输出内容
        log.info("流程定义id：{}", processInstance.getProcessDefinitionId());
        log.info("流程实例id：{}", processInstance.getId());
        log.info("当前活动Id：{}", processInstance.getActivityId());
    }

    /**
     * 查询当前个人待执行的任务
     *  流程实例id：bb67a7e4-f16b-11ec-8308-00ff7b7dfee4
     * 任务id：bb6c62da-f16b-11ec-8308-00ff7b7dfee4
     * 任务负责人：student
     * 任务名称：学生发起请假
     *
     *
     * 流程实例id：bb67a7e4-f16b-11ec-8308-00ff7b7dfee4
     * 任务id：fcaa1688-f16c-11ec-9fd2-00ff7b7dfee4
     * 任务负责人：bz
     * 任务名称：班长审批
     */
    @Test
    public void testFindPersonalTaskList() {
        // 任务负责人
        String assignee = "bz";
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("Leavecond") //流程Key
                .taskAssignee(assignee)//只查询该任务负责人的任务
                .list();
        for (Task task : list) {
            log.info("流程实例id：{}", task.getProcessInstanceId());
            log.info("任务id：{}", task.getId());
            log.info("任务负责人：{}", task.getAssignee());
            log.info("任务名称：{}", task.getName());
        }
    }

    // 完成任务
    @Test
    public void completTask() {
        // 根据流程key 和 任务的负责人 查询任务
        // 返回一个任务对象
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("Leavecond") //流程Key
                .taskAssignee("student") //要查询的负责人
                .singleResult();

        HashMap<String, Object> variables = new HashMap<>();
        variables.put("day", 1);

        // 完成任务,参数：任务id
        taskService.complete(task.getId(),variables);
    }


    //根据流程实例获取当前活动任务
    @Test
    public  void queryTask(){
        String instanceId="52a52b91-f170-11ec-8fe6-00ff7b7dfee4";
        Task  task = taskService.createTaskQuery()
                .processInstanceId(instanceId)
                .active()
                .singleResult();
        log.info("task:{}",task);
        String taskId = task.getId();
        log.info("任务ID:{}",taskId);
    }

    /**
     * 查询流程定义
     */
    @Test
    public void queryProcessDefinition() {
        // 得到ProcessDefinitionQuery 对象
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        // 查询出当前所有的流程定义
        // 条件：processDefinitionKey =evection
        // orderByProcessDefinitionVersion 按照版本排序
        // desc倒叙
        // list 返回集合
        List<ProcessDefinition> definitionList =
                processDefinitionQuery.processDefinitionKey("myLeave")
                        .orderByProcessDefinitionVersion()
                        .desc()
                        .list();
        // 输出流程定义信息
        for (ProcessDefinition processDefinition : definitionList) {
            log.info("流程定义 id= {}" , processDefinition.getId());
            log.info("流程定义name= {}" , processDefinition.getName());
            log.info("流程定义 key= {}" ,processDefinition.getKey());
            log.info("流程定义Version= {}",processDefinition.getVersion());
            log.info("流程部署ID= {}", processDefinition.getDeploymentId());
        }
    }

    /**
     *        34b61eb4-f149-11ec-ad4f-00ff7b7dfee4
     *        328b285f-f149-11ec-ad4f-00ff7b7dfee4
     */
    @Test
    public void deleteDeployment() {
        // 流程部署id
        String deploymentId = "34b61eb4-f149-11ec-ad4f-00ff7b7dfee4";
        //删除流程定义，如果该流程定义已有流程实例启动则删除时出错
//        repositoryService.deleteDeployment(deploymentId);
//        //设置true 级联删除流程定义，即使该流程有流程实例启动也可以删除，设置为false非级连删除方式
        repositoryService.deleteDeployment(deploymentId, true);
    }

}
