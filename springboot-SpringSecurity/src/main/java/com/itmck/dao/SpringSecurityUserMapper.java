package com.itmck.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.pojo.UserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/19 10:29
 **/
@Mapper
public interface SpringSecurityUserMapper extends BaseMapper<UserInfo> {

}
