package com.itmck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/19 9:30
 **/
@SpringBootApplication
public class SpringBootSpringSecurityApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringBootSpringSecurityApplication.class, args);
    }
}