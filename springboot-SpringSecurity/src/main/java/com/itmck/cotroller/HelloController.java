package com.itmck.cotroller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    //localhost:8080/hello/security
    @GetMapping("/hello/security")
    public String getWelcomeMsg() {
        return "Hello,Spring Security";
    }


    //localhost:8080/hello/helloAdmin
    @GetMapping("/hello/helloAdmin")
    @PreAuthorize("hasAnyRole('admin')")
    public String helloAdmin() {
        return "Hello,admin";
    }


    //localhost:8080/hello/helloUser
    @GetMapping("/hello/helloUser")
    @PreAuthorize("hasAnyRole('admin','normal')")
    public String helloUser() {
        return "Hello,user";
    }

    //localhost:8080/mck
    @GetMapping("/mck")
    public String mck() {
        return "Hello,mck";
    }
}