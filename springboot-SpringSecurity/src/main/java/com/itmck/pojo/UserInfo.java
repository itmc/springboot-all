package com.itmck.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/19 10:28
 **/
@Data
@TableName("spring_security_user")
public class UserInfo {


    @TableId(value = "id",type = IdType.AUTO)
    private int id;
    private String username;//用户名.
    private String password;//密码.
    private String role;
}