# Springboot学习

## @Value注解

```yaml
mck:
  name: 'miaochangke'
  list: mck,wxp,hz,hs,hf
  set: hz,hs,hf
  map: '{"name": "zhangsan", "sex": "male"}'
```
- 单值
```text
@Value("${mck.name:mck}")
private String name;
```
- list,set,map
```text
    @Value("#{'${mck.list}'.split(',')}")
    List<String> list;

    @Value("#{'${mck.set}'.empty ? null : '${mck.set}'.split(',')}")
    Set<String> set;

    @Value("#{${mck.map}}")
    Map<String, String> map;

```
- 静态
```java
@Component
public class MySpring3 {

    public static String name;

    @Value("${mck.name:mck}")
    public void setName(String name) {
        MySpring3.name = name;
    }

    public  String getName() {
        return name;
    }
}
```
## @ConfigurationProperties

```yaml
mck:
  name: 'miaochangke'
  address:
    - 10
    - 20
    - 30
```

```text
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>
```
```java
package com.itmc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "mck")
public class MySpring2 {

    private String name;
    List<String> address;
}
```
