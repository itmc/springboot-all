package com.itmc;

import com.itmc.config.MySpring;
import com.itmc.config.MySpring2;
import com.itmc.config.MySpring3;
import com.itmc.config.MySpring4;
import com.itmc.imp.UploadBase;
import com.itmc.imp.UploadFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/23 14:41
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootBasicStudyApplicationTest {


    @Resource
    private MySpring mySpring;

    @Resource
    private MySpring2 mySpring2;

    @Resource
    private MySpring3 mySpring3;

    @Resource
    private MySpring4 mySpring4;


    @Test
    public void valueMethod(){
        String name = mySpring.getName();
        log.info("name:{}",name);
    }

    @Test
    public void valueMethod2(){
        String name = mySpring2.getName();
        log.info("name:{}",name);

        List<String> list = mySpring2.getAddress();
        log.info("list:{}",list);
    }

    @Test
    public void valueStaticMethod(){
        String name = mySpring3.getName();
        log.info("name:{}",name);
    }

    @Test
    public void valueSpelMethod(){
        List<String> list = mySpring4.getList();
        log.info("list:{}",list);
        Set<String> set = mySpring4.getSet();
        log.info("set:{}",set);
        Map<String,String> map = mySpring4.getMap();
        log.info("map:{}",map);
    }



    @Test
    public void  runs(){

        UploadBase aUpload = UploadFactory.newInstance("aUpload");
        if (aUpload!=null){
            aUpload.upload("mck");
        }
    }


}