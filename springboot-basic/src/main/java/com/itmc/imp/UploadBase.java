package com.itmc.imp;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;


/**
 * 太阳当空照，花儿对我笑
 * <p>
 * Create by M ChangKe 2022/8/5 15:55
 *
 * InitializingBean 扩展点的使用
 */
@Component
public interface UploadBase extends InitializingBean {

    void upload(String str);
}
