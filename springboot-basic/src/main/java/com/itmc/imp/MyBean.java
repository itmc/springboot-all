package com.itmc.imp;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class MyBean implements InitializingBean {

    public MyBean() {
        System.out.println("执行MyBean构造");
    }

    public void afterPropertiesSet() throws Exception {

        System.out.println("执行afterPropertiesSet()");
    }
}
