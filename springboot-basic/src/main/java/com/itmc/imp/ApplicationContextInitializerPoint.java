package com.itmc.imp;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Iterator;


public class ApplicationContextInitializerPoint implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    public void initialize(ConfigurableApplicationContext applicationContext) {
        System.out.println("------------ApplicationContextInitializerPoint # initialize 开始-------------");
        System.out.println("[ApplicationContextInitializer扩展点演示] # initialize:  " + applicationContext.toString());
        System.out.println("BeanDefinitionCount count: " + applicationContext.getBeanDefinitionCount());
        ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();
        Iterator<String> beanNamesIterator = beanFactory.getBeanNamesIterator();
        if (beanNamesIterator.hasNext()) {
            beanNamesIterator.forEachRemaining(System.out::println);
        }

        System.out.println("时机: "+ "run 方法中的 this.prepareContext(); 的时候");
        System.out.println("-------------ApplicationContextInitializerPoint # initialize 结束------------");
        System.out.println();
    }
}
