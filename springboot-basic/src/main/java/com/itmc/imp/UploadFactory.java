package com.itmc.imp;

import java.util.HashMap;
import java.util.Map;

public class UploadFactory {

    public static Map<String,UploadBase> hod=new HashMap<String, UploadBase>();


    public static UploadBase newInstance(String uploadName){

        if (hod.containsKey(uploadName)) {
            return hod.get(uploadName);
        }
        return null;
    }

}
