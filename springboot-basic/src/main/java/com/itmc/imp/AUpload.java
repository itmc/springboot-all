package com.itmc.imp;

import org.springframework.stereotype.Component;

@Component
public class AUpload implements UploadBase{


    public void upload(String str) {
        System.out.println("AUpload:"+str);
    }

    public void afterPropertiesSet() throws Exception {
        UploadFactory.hod.put("aUpload",this);
    }
}
