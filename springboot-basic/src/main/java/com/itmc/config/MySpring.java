package com.itmc.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/23 14:40
 *
 *
 * 读取配置文件数据方式一 @Value
 **/
@Data
@Component
public class MySpring {

    /**
     *
     * 如果配置文件缺少默认值,则启动报错,这里可以使用 :设置默认值,如下
     * @Value("${mck.name:mck}") 如果默认值为空则会取 mck.如果不需要默认值 : 后边设置空即可@Value("${mck.name:}")
     */
    @Value("${mck.name:mck}")
    private String name;
}