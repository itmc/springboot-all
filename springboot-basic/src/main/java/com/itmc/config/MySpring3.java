package com.itmc.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/23 14:40
 * <p>
 * <p>
 * 读取配置文件数据方式一 @Value
 *
 * 如果在静态变量上使用@Value则注入失败,返回null.使用下面方式即可
 **/
@Component
public class MySpring3 {

    public static String name;

    @Value("${mck.name:mck}")
    public void setName(String name) {
        MySpring3.name = name;
    }

    public  String getName() {
        return name;
    }
}