package com.itmc.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/23 14:40
 * <p>
 * <p>
 * 读取配置文件数据方式一 @Value
 * <p>
 * 如果在静态变量上使用@Value 结合spel表达式
 * <p>
 * set和list类似  另外设置默认值如下
 **/
@Component
@Data
public class MySpring4 {


    @Value("#{'${mck.list}'.split(',')}")
    List<String> list;

    @Value("#{'${mck.set}'.empty ? null : '${mck.set}'.split(',')}")
    Set<String> set;

    @Value("#{${mck.map}}")
    Map<String, String> map;


}