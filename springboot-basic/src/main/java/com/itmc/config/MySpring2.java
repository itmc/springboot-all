package com.itmc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/23 14:40
 * <p>
 * <p>
 * 读取配置文件数据方式一 @ConfigurationProperties
 **/
@Data
@Configuration
@ConfigurationProperties(prefix = "mck")
public class MySpring2 {

    private String name;


    List<String> address;
}