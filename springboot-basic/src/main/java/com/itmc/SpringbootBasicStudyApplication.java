package com.itmc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/23 14:38
 **/
@RestController
@SpringBootApplication
public class SpringbootBasicStudyApplication {

    public static void main(String[] args) {

        SpringApplication.run(SpringbootBasicStudyApplication.class, args);

    }



    @Value(value = "${server.port}")
    private String port;


    @GetMapping("/port")
    public String getPort(){
        return  "hello: "+port;
    }
}