//package com.itmck.es;
//
//
//import com.alibaba.fastjson.JSON;
//import com.itmck.es.dto.User;
//import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
//import org.elasticsearch.action.bulk.BulkRequest;
//import org.elasticsearch.action.bulk.BulkResponse;
//import org.elasticsearch.action.delete.DeleteRequest;
//import org.elasticsearch.action.delete.DeleteResponse;
//import org.elasticsearch.action.get.GetRequest;
//import org.elasticsearch.action.get.GetResponse;
//import org.elasticsearch.action.index.IndexRequest;
//import org.elasticsearch.action.index.IndexResponse;
//import org.elasticsearch.action.search.SearchRequest;
//import org.elasticsearch.action.search.SearchResponse;
//import org.elasticsearch.action.support.master.AcknowledgedResponse;
//import org.elasticsearch.action.update.UpdateRequest;
//import org.elasticsearch.action.update.UpdateResponse;
//import org.elasticsearch.client.RequestOptions;
//import org.elasticsearch.client.RestHighLevelClient;
//import org.elasticsearch.client.indices.CreateIndexRequest;
//import org.elasticsearch.client.indices.CreateIndexResponse;
//import org.elasticsearch.client.indices.GetIndexRequest;
//import org.elasticsearch.common.unit.TimeValue;
//import org.elasticsearch.common.xcontent.XContentType;
//import org.elasticsearch.index.query.QueryBuilders;
//import org.elasticsearch.rest.RestStatus;
//import org.elasticsearch.search.SearchHit;
//import org.elasticsearch.search.builder.SearchSourceBuilder;
//import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import javax.annotation.Resource;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.concurrent.TimeUnit;
//
//@SpringBootTest
//public class EsTest {
//
//    @Resource
//    private RestHighLevelClient restHighLevelClient;
//
//    @Test
//    void testCreateIndex() throws IOException {
//
//        CreateIndexRequest request = new CreateIndexRequest("mc_book");
//        CreateIndexResponse createIndexResponse = restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);
//        System.out.println(createIndexResponse);
//    }
//
//    @Test
//    void testExistsIndex() throws IOException {
//        GetIndexRequest request = new GetIndexRequest("mc_book");
//        boolean exists = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
//        System.out.println(exists);
//    }
//
//    @Test
//    void deleteIndex() throws IOException {
//
//        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest("mc_book");
//        AcknowledgedResponse delete = restHighLevelClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
//        System.out.println(JSON.toJSONString(delete));
//    }
//
//    @Test
//    public void docExists() throws IOException {
//        GetRequest getRequest = new GetRequest("mc_book", "1");
//        //只判断索引是否存在不需要获取_source
//        getRequest.fetchSourceContext(new FetchSourceContext(false));
//        getRequest.storedFields("_none_");
//        boolean exists = restHighLevelClient.exists(getRequest, RequestOptions.DEFAULT);
//        System.out.println(exists);
//    }
//
//    @Test
//    public void addDocument() throws IOException {
//
//        User user = new User("mck", 1);
//        IndexRequest request = new IndexRequest("mc_book");
//        request.id("1");
//        request.timeout(TimeValue.timeValueSeconds(1));
//        request.timeout("1s");
//        request.source(JSON.toJSONString(user), XContentType.JSON);
//        // 发送请求
//        IndexResponse indexResponse = restHighLevelClient.index(request, RequestOptions.DEFAULT);
//        System.out.println(indexResponse.toString());
//        RestStatus Status = indexResponse.status();
//        System.out.println(Status == RestStatus.OK || Status == RestStatus.CREATED);
//
//    }
//
//    // 判断此id是否存在这个索引库中
//    @Test
//    void testIsExists() throws IOException {
//        GetRequest getRequest = new GetRequest("mc_book", "1");
//        // 不获取_source上下文 storedFields
//        getRequest.fetchSourceContext(new FetchSourceContext(false));
//        getRequest.storedFields("_none_");
//        // 判断此id是否存在！
//        boolean exists = restHighLevelClient.exists(getRequest, RequestOptions.DEFAULT);
//        System.out.println(exists);
//    }
//
//    // 获得文档记录
//    @Test
//    void testGetDocument() throws IOException {
//        GetRequest getRequest = new GetRequest("mc_book", "1");
//        GetResponse getResponse = restHighLevelClient.get(getRequest,RequestOptions.DEFAULT);
//        System.out.println(getResponse.getSourceAsString()); // 打印文档内容
//        System.out.println(getResponse);
//    }
//
//    // 更新文档记录
//    @Test
//    void testUpdateDocument() throws IOException {
//        UpdateRequest request = new UpdateRequest("mc_book", "1");
//        request.timeout(TimeValue.timeValueSeconds(1));
//        request.timeout("1s");
//        User user = new User("狂神说", 18);
//        request.doc(JSON.toJSONString(user), XContentType.JSON);
//        UpdateResponse updateResponse = restHighLevelClient.update(request, RequestOptions.DEFAULT);
//        System.out.println(updateResponse.status() == RestStatus.OK);
//    }
//
//    // 删除文档测试
//    @Test
//    void testDelete() throws IOException {
//        DeleteRequest request = new DeleteRequest("mc_book", "1");
//        //timeout
//        request.timeout(TimeValue.timeValueSeconds(1));
//        request.timeout("1s");
//        DeleteResponse deleteResponse = restHighLevelClient.delete(request, RequestOptions.DEFAULT);
//        System.out.println(deleteResponse.status() == RestStatus.OK);
//    }
//
//    // 批量添加数据
//    @Test
//    void testBulkRequest() throws IOException {
//        BulkRequest bulkRequest = new BulkRequest();
//        //timeout
//        bulkRequest.timeout(TimeValue.timeValueMinutes(2));
//        bulkRequest.timeout("2m");
//        ArrayList<User> userList = new ArrayList<>();
//        userList.add(new User("mck", 3));
//        userList.add(new User("mck1", 3));
//        userList.add(new User("mck2", 3));
//        userList.add(new User("mck3", 3));
//        userList.add(new User("mck4", 3));
//        userList.add(new User("mck5", 3));
//        for (int i = 0; i < userList.size(); i++) {
//            bulkRequest.add(new IndexRequest("mc_book")
//                    .id("" + (i + 1))
//                    .source(JSON.toJSONString(userList.get(i)), XContentType.JSON));
//        }
//        // bulk
//        BulkResponse bulkResponse = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
//        System.out.println(!bulkResponse.hasFailures());
//    }
//
//    @Test
//    void testSearch() throws IOException {
//        SearchSourceBuilder sourceBuilder = SearchSourceBuilder
//                .searchSource()
//                .query(QueryBuilders.matchAllQuery())//查询所有
////                .query(QueryBuilders.termQuery("name", "mck"))//精确匹配
////                .query(QueryBuilders.simpleQueryStringQuery("mck1"))
//                .query(QueryBuilders.matchQuery("name","mck"))
//                .timeout(new TimeValue(60, TimeUnit.SECONDS));
//
//        SearchRequest searchRequest = new SearchRequest("mc_book");
//        searchRequest.source(sourceBuilder);
//        SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
//        System.out.println(JSON.toJSONString(response.getHits()));
//        System.out.println("================SearchHit==================");
//        for (SearchHit documentFields : response.getHits().getHits()) {
//            System.out.println(documentFields.getSourceAsMap());
//        }
//    }
//}
