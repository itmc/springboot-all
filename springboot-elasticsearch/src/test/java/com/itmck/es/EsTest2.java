package com.itmck.es;


import com.itmck.es.dto.Document;
import com.itmck.es.mapper.DocumentMapper;
import org.dromara.easyes.core.biz.EsPageInfo;
import org.dromara.easyes.core.core.EsWrappers;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest(classes = SpringbootEsApplication.class)
@RunWith(SpringRunner.class)
public class EsTest2 {

    @Resource
    private DocumentMapper documentMapper;

    @Test
    public void testCreateIndex() {
        // 测试创建索引,框架会根据实体类及字段上加的自定义注解一键帮您生成索引 需确保索引托管模式处于manual手动挡(默认处于此模式),若为自动挡则会冲突
        boolean success = documentMapper.createIndex();
        Assertions.assertTrue(success);
    }

    @Test
    public void testInsert() {
        // 测试插入数据
        Document document = new Document();
        document.setTitle("老汉2");
        document.setContent("推*技术过硬2");
        int successCount = documentMapper.insert(document);
        System.out.println(successCount);
    }


    @Test
    public void testSelect() {
        // 测试查询 写法和MP一样 可以用链式,也可以非链式 根据使用习惯灵活选择即可
        String title = "老汉2";
        Document document = EsWrappers.lambdaChainQuery(documentMapper)
                .eq(Document::getTitle, title)
                .one();
        System.out.println(document);
        Assertions.assertEquals(title,document.getTitle());
    }

    @Test
    public void testSelectPage() {
        EsPageInfo<Document> page = EsWrappers.lambdaChainQuery(documentMapper)
                .page(1, 3);
        System.out.println(page.getList());
    }

}
