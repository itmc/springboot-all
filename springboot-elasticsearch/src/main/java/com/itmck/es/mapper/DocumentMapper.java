package com.itmck.es.mapper;

import com.itmck.es.dto.Document;
import org.dromara.easyes.core.core.BaseEsMapper;

public interface DocumentMapper extends BaseEsMapper<Document> {
}
