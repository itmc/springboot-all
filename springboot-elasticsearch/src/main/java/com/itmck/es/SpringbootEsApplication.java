package com.itmck.es;


import org.dromara.easyes.starter.register.EsMapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EsMapperScan("com.itmck.es.mapper")
public class SpringbootEsApplication {


    public static void main(String[] args) {

        SpringApplication.run(SpringbootEsApplication.class, args);
    }
}
