package com.itmck.springboot;

import com.itmck.springboot.config.WebSocket;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Hello world!
 */
@SpringBootApplication
@RestController
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Resource
    private WebSocket webSocket;

    @GetMapping("/send/{msg}")
    public String hello(@RequestParam(value = "user", required = false) String user, @PathVariable("msg") String msg) {
        if (!"".equals(user) && null != user) {
            webSocket.sendOneMessage(user, msg);
        } else {
            webSocket.sendAllMessage(msg);
        }
        return msg;
    }
}

