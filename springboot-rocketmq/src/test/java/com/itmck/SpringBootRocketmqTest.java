package com.itmck;

import com.itmck.component.RocketMqComponent;
import com.itmck.domain.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootRocketmqTest {

    @Autowired
    private RocketMqComponent rocketMqComponent;

    @Test
    public void testProducter() {
        Person person = new Person();
        person.setName("heyuhua");
        person.setAge(25);
        rocketMqComponent.asyncSend("PERSON_ADD", MessageBuilder.withPayload(person).build());

    }


}