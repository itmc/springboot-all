package com.itmck.producer;

import com.alibaba.fastjson.JSON;
import com.itmck.component.RocketMqComponent;
import com.itmck.domain.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class RocketMqProducerController {


    @Resource
    private RocketMqComponent rocketMqComponent;

    @GetMapping("/send")
    public String testSendMsg() {
        Person perSon = new Person()
                .setName("mck")
                .setAge(20);
        rocketMqComponent.sendMessage("PERSON_ADD", JSON.toJSONString(perSon));
        return "send message success";
    }

}
