package com.itmck;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRocketmq {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootRocketmq.class, args);
    }
}