# Springboot集成拦截器以及过滤器

## 拦截器与过滤器的区别?
> 简单描述:过滤器是过滤掉一些不需要的东西,拦截器是你希望干预它的进展，甚至终止它进行

## 拦截器与过滤器的区别总结:
- ①：拦截器是基于java的反射机制的，而过滤器是基于函数的回调。
- ②：拦截器不依赖于servlet容器，而过滤器依赖于servlet容器。
- ③：拦截器只对action请求起作用，而过滤器则可以对几乎所有的请求起作用。
- ④：拦截器可以访问action上下文、值、栈里面的对象，而过滤器不可以。
- ⑤：在action的生命周期中，拦截器可以多次被调用，而过滤器只能在容器初始化时被调用一次。
- ⑥：拦截器可以获取IOC容器中的各个bean，而过滤器不行，这点很重要，在拦截器里注入一个service，可以调用业务逻辑。

## 拦截器与过滤器的触发时机
过滤器是在请求进入容器后，但请求进入servlet之前进行预处理的。请求结束返回也是，是在servlet处理完后，返回给前端之前。
过滤器包裹servlet，servlet包裹住拦截器

## springboot中如何使用Filter过滤器?
- 方式一
> @Order里边的数字越小代表越先被该Filter过滤，@WebFilter代表这是个Filter类并把这个类注入到容器中

步骤一:
```java
@Slf4j
@Order(3)
@WebFilter(filterName ="myFilter3" ,urlPatterns = "/*")
public class MyFilter3 implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.info("MyFilter3---开始");
        chain.doFilter(request,response);
    }
}
```
步骤二:
> 启动类添加注解:@ServletComponentScan
```java
@ServletComponentScan
@SpringBootApplication
public class SpringbootFilterInterceptorApp {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootFilterInterceptorApp.class, args);
    }
}
```
- 方式二

步骤一:
```java
@Slf4j
public class MyFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.info("执行过滤器MyFilter---开始");
        chain.doFilter(request,response);
    }

}
```
步骤二:
```java
@Configuration
public class MyConfig {


    @Bean
    MyFilter myFilter() {
        return new MyFilter();
    }

    @Bean
    public FilterRegistrationBean<MyFilter> filterRegistrationBean(MyFilter myFilter) {
        FilterRegistrationBean<MyFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(myFilter);
        filterRegistrationBean.setOrder(1);
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

}
```

## springboot中如何使用Interceptor拦截器?

使用拦截器步骤:
```
创建拦截器实现HandlerInterceptor接口或者继承HandlerInterceptorAdapter类
   继承关系:HandlerInterceptorAdapter--->AsyncHandlerInterceptor-->HandlerInterceptor
   通过继承关系可知,我们可以直接通过继承拦截器适配器类来创建自定义的拦截器,这样需要哪个方法就直接重写哪个即可
自定义配置类继承 WebMvcConfigurerAdapter 类或者 WebMvcConfigurationSupport类
    同样道理,使用适配器类
```

步骤一
```java
@Slf4j
public class MyInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("进入拦截器之前>>>>>>>>>>>>>");

        String name = request.getParameter("name");
        if (name.equals("mck")){
           return true;
        }
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

        log.info("进入拦截器中>>>>>>>>>>>>>");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

        log.info("拦截器结束>>>>>>>>>>>>>");
    }
}
```

步骤二:
```java
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {


    @Bean
    public MyInterceptor getMyInterceptor() {
        return new MyInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(getMyInterceptor())
                .addPathPatterns("/**")//// 拦截所有路径
                .excludePathPatterns("/system/max/login");// 不拦截的路径
    }
}
```


