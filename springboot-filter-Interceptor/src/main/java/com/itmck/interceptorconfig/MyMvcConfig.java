package com.itmck.interceptorconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/12/5 20:12
 **/
@Configuration
public class MyMvcConfig implements WebMvcConfigurer {


    @Bean
    public MyInterceptor getMyInterceptor() {
        return new MyInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(getMyInterceptor())
                .addPathPatterns("/**")//// 拦截所有路径
                .excludePathPatterns("/system/max/login");// 不拦截的路径
    }
}