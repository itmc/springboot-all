package com.itmck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/12/5 19:39
 **/
@ServletComponentScan
@SpringBootApplication
public class SpringbootFilterInterceptorApp {


    public static void main(String[] args) {
        SpringApplication.run(SpringbootFilterInterceptorApp.class, args);
    }
}