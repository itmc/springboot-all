package com.itmck.filterconfig;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/12/5 19:42
 **/
@Slf4j
@Order(3)
@WebFilter(filterName ="myFilter3" ,urlPatterns = "/*")
public class MyFilter3 implements Filter {


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.info("MyFilter3---开始");
        chain.doFilter(request,response);
    }

}