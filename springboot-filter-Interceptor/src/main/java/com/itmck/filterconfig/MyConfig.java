package com.itmck.filterconfig;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/12/5 19:45
 *
 *
 * springboot集成过滤器方式1
 **/

@Configuration
public class MyConfig {


    @Bean
    MyFilter myFilter() {
        return new MyFilter();
    }

    @Bean
    MyFilter2 myFilter2() {
        return new MyFilter2();
    }

    @Bean
    public FilterRegistrationBean<MyFilter> filterRegistrationBean(MyFilter myFilter) {
        FilterRegistrationBean<MyFilter> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(myFilter);
        filterRegistrationBean.setOrder(1);
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }

    @Bean
    public FilterRegistrationBean<MyFilter2> filterRegistrationBean2(MyFilter2 myFilter2) {
        FilterRegistrationBean<MyFilter2> filterRegistrationBean = new FilterRegistrationBean<>();
        filterRegistrationBean.setFilter(myFilter2);
        filterRegistrationBean.setOrder(2);
        filterRegistrationBean.addUrlPatterns("/*");
        return filterRegistrationBean;
    }


}