package com.itmck.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/12/5 19:40
 **/
@RestController
public class MyController {

    @RequestMapping("/hello")
    public String hello(String name) {
        return name;
    }
}