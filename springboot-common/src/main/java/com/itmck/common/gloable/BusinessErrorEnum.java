package com.itmck.common.gloable;

public enum BusinessErrorEnum implements TopEnum {

    system_error("500", "系统异常");
    final String code;

    final String message;


    BusinessErrorEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }
}
