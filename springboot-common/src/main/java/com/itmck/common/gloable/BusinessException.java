package com.itmck.common.gloable;


import lombok.Getter;
import lombok.Setter;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/3 10:24
 **/
@Getter
@Setter
public class BusinessException extends RuntimeException {

    private String code = "500";
    private String message = "system exception";

    public BusinessException(TopEnum topEnum) {
        super(topEnum.message());
        this.code = topEnum.code();
    }

    public BusinessException(String message, String code) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public BusinessException(String message) {
        super(message);
        this.message = message;
    }
}