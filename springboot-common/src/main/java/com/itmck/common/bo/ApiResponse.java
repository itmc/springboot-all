package com.itmck.common.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 通用响应类
 * create by it_mck·Miao
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse<T> implements Serializable {


    private static final long serialVersionUID = -7123165055847770225L;

    /**
     * 错误码
     */
    private String code;
    /**
     * 错误信息
     */
    private String message;

    /**
     * 响应数据
     */
    private T data;
    /**
     * 成功与失败,默认成功
     */
    private boolean success;

    public static <T> ApiResponse<T> ok(T data) {
        //返回接口成功信息
        return new ApiResponse<>("200", "", data, true);
    }

    public static <T> ApiResponse<T> error(String code, String message) {
        //返回失败信息
        return new ApiResponse<>(code, message, null, false);
    }
}
