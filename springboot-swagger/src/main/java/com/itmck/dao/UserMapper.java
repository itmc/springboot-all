package com.itmck.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itmck.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 太阳当空照,花儿对我笑
 * 使用Mybatis-plus通用mapper
 * Create by M ChangKe 2021/11/16 9:39
 **/
@Repository
@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Select(value = "select * from lc_user ${ew.customSqlSegment}")
    User selectByName(@Param("ew") Wrapper<User> updateWrapper);


    int insertBatchSomeColumn(List<T> entityList);

}
