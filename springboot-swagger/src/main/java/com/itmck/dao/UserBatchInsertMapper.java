package com.itmck.dao;

import com.itmck.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * @author xg BLACK
 * @date 2022/11/25 11:08
 * description:
 */
@Mapper
@Component
public interface UserBatchInsertMapper extends SpiceBaseMapper<User> {
 
}