package com.itmck.pojo;

import lombok.Data;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/15 16:14
 **/
@Data
public class UserDTO {

    private Long userId;
    private String userName;
    private Integer password;
    private String status;

}