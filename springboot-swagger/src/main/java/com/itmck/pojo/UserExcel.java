package com.itmck.pojo;


import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserExcel {

    @ExcelProperty(value = "编号", index = 0)
    private String userName;

    @ExcelProperty(value = "地址", index = 1)
    private String password;
}
