package com.itmck.pojo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel
@Data
public class StudentDTO {

    @ApiModelProperty(name = "name",value = "姓名",example = "itmck")
    private String name;

    @ApiModelProperty(name = "address",value = "地址",example = "浙江杭州")
    private String address;

}
