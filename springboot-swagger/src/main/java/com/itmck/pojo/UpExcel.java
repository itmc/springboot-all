package com.itmck.pojo;


import com.alibaba.excel.annotation.ExcelProperty;
import com.easy.tool.mybatisplustool.annotation.Wrapper;
import com.easy.tool.mybatisplustool.annotation.WrapperType;
import lombok.Data;

@Data
public class UpExcel {

    @ExcelProperty(value = "SERIAL_NUMBER", index = 0)
    private String userName;

    @ExcelProperty(value = "STATUS", index = 1)
    private String password;

}
