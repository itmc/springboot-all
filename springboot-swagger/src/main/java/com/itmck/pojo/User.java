package com.itmck.pojo;

import com.baomidou.mybatisplus.annotation.*;
import com.easy.tool.mybatisplustool.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/15 16:14
 **/
@Data
@Accessors(chain = true)
@TableName(value = "lc_user")

@WrapperGroups(groups = {
        @Group(group = "group1", conditionType = ConditionType.OR, splicingType = ConditionType.AND),
        @Group(group = "group2", conditionType = ConditionType.AND, splicingType = ConditionType.AND)
})
public class User {

    @TableId(value = "USER_ID", type = IdType.AUTO)
    private Long userId;

    //    @TableField(value = "USER_NAME",whereStrategy = FieldStrategy.NOT_EMPTY,condition = SqlCondition.EQUAL)
    @Wrapper(value = WrapperType.LIKE, group = "group1")
    private String userName;

    //    @TableField(value ="PASSWORD",whereStrategy = FieldStrategy.NOT_EMPTY,condition = SqlCondition.EQUAL)
    @Wrapper(group = "group1")
    private String password;

    //    @TableField("STATUS")
    @Wrapper(group = "group2")
    private String status;

}