package com.itmck.pojo;

import com.alibaba.excel.annotation.ExcelProperty;

import java.io.Serializable;


public class ImplementInfoExcel implements Serializable {

    private static final long serialVersionUID = 1L;

    @ExcelProperty(value = "序列号", index = 0)
    private String serialNo;

    @ExcelProperty(value = "设备型号", index = 1)
    private String modelNo;

    @ExcelProperty(value = "设备类型", index = 2)
    private String terminalType;

    @ExcelProperty(value = "二维码牌url", index = 3)
    private String quickCodeUrl;

    @ExcelProperty(index = 4)
    private String bType;

    @ExcelProperty(index = 5)
    private String devFac;


    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public String getModelNo() {
        return modelNo;
    }

    public void setModelNo(String modelNo) {
        this.modelNo = modelNo;
    }

    public String getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType;
    }

    public String getQuickCodeUrl() {
        return quickCodeUrl;
    }

    public void setQuickCodeUrl(String quickCodeUrl) {
        this.quickCodeUrl = quickCodeUrl;
    }

    public String getbType() {
        return bType;
    }

    public void setbType(String bType) {
        this.bType = bType;
    }

    public String getDevFac() {
        return devFac;
    }

    public void setDevFac(String devFac) {
        this.devFac = devFac;
    }
}
