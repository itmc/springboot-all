package com.itmck.dto;


import lombok.Data;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

@Data
public class DataSwMbqcdssDO {

    private String name;


    public static void main(String[] args) {


        try {
            extracted();
        } catch (Exception e) {
            System.out.println("出错了");
        }


//        //测试Function函数
//        Integer apply = apply(i -> i * 4, 3);
//        System.out.println(apply);
//
//        //测试Supplier函数
//        supplier(() -> "Hello Jack!");
//
//        //测试Consumer
//        consumer(s -> System.out.println("hello:" + s), "mck");
    }

    private static void extracted() {
        try {
           int i=1/0;
        } finally {
            System.out.println("finally");
        }
    }

    public static Integer apply(Function<Integer, Integer> function, Integer s) {
        return function.apply(s);
    }


    public static void supplier(Supplier<String> supplier) {
        String s = supplier.get();
        System.out.println("s");
    }

    public static void consumer(Consumer<String> consumer, String str) {
        consumer.accept(str);
    }
}
