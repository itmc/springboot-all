package com.itmck.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @author xg BLACK
 * @date 2022/11/1 15:20
 * description:
 */
@ApiModel("管理后台 - 数据导入 Response VO")
@Data
@Builder
public class DataSwMbqcdssImportExcelVO {
    @ApiModelProperty(value = "导入成功的数量")
    private Integer successCount;
 
    @ApiModelProperty(value = "导入失败的数量")
    private Integer failCount;
 
    @ApiModelProperty(value = "导入用时")
    private Long time;
}