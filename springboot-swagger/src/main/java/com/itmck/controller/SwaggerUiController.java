package com.itmck.controller;

import com.itmck.common.bo.ApiResponse;
import com.itmck.pojo.StudentDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("/swagger")
@Api(value = "SwaggerUiController", tags = "swagger教程")
public class SwaggerUiController {

    @ApiOperation(value = "swagger无参get测试", notes = "swagger无参get测试")
    @GetMapping("/hello")
    public ApiResponse<String> sayHello() {

        return ApiResponse.ok("hello java");
    }

    @ApiOperation(value = "swagger有参表单提交get测试", notes = "swagger有参表单提交get测试")
    @GetMapping("hello2")
    public ApiResponse<StudentDTO> helloSwagger2(StudentDTO student) {
        return ApiResponse.ok(student);
    }

    @ApiOperation(value = "swagger有参表单提交get测试", notes = "swagger有参表单提交get测试")
    @PostMapping("hello22")
    public ApiResponse<StudentDTO> helloSwaggerPost(@RequestBody StudentDTO student) {
        return ApiResponse.ok(student);
    }

    @ApiOperation(value = "swagger测试3", notes = "swagger使用演示3")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "姓名", dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "age", value = "年龄", dataType = "Integer", paramType = "path")
    })
    @GetMapping("hello3/{name}/{age}")
    public ApiResponse<String> helloSwagger3(@PathVariable String name, @PathVariable int age) {
        return ApiResponse.ok("my name is:" + name + " and age is : " + age);
    }

    @ApiOperation(value = "swagger测试4", notes = "swagger使用演示4")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "姓名", dataType = "String", paramType = "path"),
            @ApiImplicitParam(name = "address", value = "地址", dataType = "String", paramType = "query")
    })
    @GetMapping("hello4/{name}")
    public ApiResponse<String> helloSwagger4(@PathVariable String name, @RequestParam String address) {

        return ApiResponse.ok("my name is:" + name + " address: " + address);
    }

    @ApiOperation(value = "上传swagger演示", notes = "swagger演示上传", consumes = "multipart/form-data", hidden = true)
    @PostMapping("hello5/{name}")
    public ApiResponse<String> helloSwagger5(@PathVariable String name, @RequestParam("qqfile") MultipartFile file) {

        String name1 = file.getName();
        return ApiResponse.ok("my name is:" + name + "正在测试上传文件:" + name1);
    }

    @ApiOperation(value = "swagger隐藏方法", notes = "swagger隐藏方法", hidden = true)
    @PostMapping("hello6/{name}")
    public ApiResponse<String> helloSwagger6(@PathVariable String name) {
        return ApiResponse.ok("my name is:" + name);
    }


}
