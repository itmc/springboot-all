package com.itmck.controller;

import com.easy.tool.mybatisplustool.response.ApiResult;
import com.easy.tool.mybatisplustool.response.ApiResultPage;
import com.easy.tool.mybatisplustool.util.MpEasyTool;
import com.itmck.dao.UserMapper;
import com.itmck.pojo.User;
import com.itmck.pojo.UserDTO;
import com.itmck.service.UserService;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/18 13:58
 **/
@Slf4j
@RestController
public class MpController {

    @Resource
    private UserMapper userMapper;

    @Resource
    private UserService userService;


    @GetMapping("mpJson")
    public ApiResultPage<UserDTO> loadPageList(HttpServletRequest request) {
        return MpEasyTool.page(request, userMapper, userQueryWrapper -> userQueryWrapper.eq("user_name", "mck"), UserDTO.class);
    }


    @GetMapping("mpJson2")
    public ApiResult<List<UserDTO>> loadList(HttpServletRequest request) {

        List<UserDTO> user2 = MpEasyTool.list(userMapper, userLambdaQueryChainWrapper -> userLambdaQueryChainWrapper.eq(User::getUserName, "mck")
                .list(), UserDTO.class);

        List<User> list = MpEasyTool.list(request, userMapper);

        List<UserDTO> list1 = MpEasyTool.list(request, userMapper, UserDTO.class);

        System.out.println(user2);
        System.out.println(list);
        System.out.println(list1);
        return null;
    }

    @GetMapping("mpJson3")
    public ApiResult<List<User>> loadList3() {



        User user = new User();
        user.setUserName("mck")
                .setPassword("123")
                .setStatus("1");

       List<User> list =  MpEasyTool.list(userMapper, user);
        return ApiResult.ok(list);
    }

    @GetMapping("mpJson4")
    public ApiResult<List<User>> loadList4(HttpServletRequest request) {

        List<User> list= userService.list(request);
        return ApiResult.ok(list);
    }
}