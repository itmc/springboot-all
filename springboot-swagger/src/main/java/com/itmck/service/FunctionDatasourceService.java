package com.itmck.service;

import com.itmck.dto.FunctionDatasourceCreateReqVO;
import com.itmck.pojo.DataExcelImportRespVO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public interface FunctionDatasourceService {
    DataExcelImportRespVO createFunctionDatasourceAndSwMbqcdss(MultipartFile file) throws IOException, ExecutionException, InterruptedException;
}
