package com.itmck.service;

import com.easy.tool.mybatisplustool.service.IMpService;
import com.itmck.pojo.User;

/**
 * Create by it_mck 2019/9/14 21:08
 *
 * @Description:
 * @Version: 1.0
 */
public interface UserService extends IMpService<User> {

}
