package com.itmck.service;

import com.itmck.pojo.DataExcelImportRespVO;
import com.itmck.pojo.UserDTO;
import com.itmck.pojo.UserExcel;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

public interface ExcelService {
    /**
     * 存储数据
     * @param cachedDataList
     */
    void saveData(List<UserExcel> cachedDataList);

}
