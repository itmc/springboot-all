package com.itmck.service.impl;

import com.easy.tool.mybatisplustool.service.impl.MpServiceImpl;
import com.itmck.dao.UserMapper;
import com.itmck.pojo.User;
import com.itmck.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl extends MpServiceImpl<UserMapper, User> implements UserService {

}
