package com.itmck.service.impl;

import com.alibaba.fastjson.JSON;
import com.itmck.dao.UserMapper;
import com.itmck.pojo.User;
import com.itmck.pojo.UserExcel;
import com.itmck.service.ExcelService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ExcelServiceImpl implements ExcelService {


    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Override
    public void saveData(List<UserExcel> cachedDataList) {

        log.info("模拟存储存储数据库:{}", JSON.toJSONString(cachedDataList));
        long start = System.currentTimeMillis();
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
        UserMapper studentMapperNew = sqlSession.getMapper(UserMapper.class);
        cachedDataList.forEach(userExcel -> {
            User user = new User();
            user.setUserName(userExcel.getUserName());
            studentMapperNew.insert(user);
        });
        sqlSession.commit();
        sqlSession.clearCache();
        System.out.println(System.currentTimeMillis() - start);

    }


}
