package com.itmck.controller;

import com.itmck.filterconfig.SaTokenConfig;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * 太阳当空照,花儿对我笑
 * <p>
 * Create by M ChangKe 2021/11/16 19:50
 **/
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class SwaggerUiControllerTest {


    @Resource
    SaTokenConfig saTokenConfig;

    @Test
    public void run() {
        log.info("swagger-ui");
    }

    @Test
    public void SaTokenConfig() {
        log.info("saTokenConfig:{}",saTokenConfig.toString());
    }

}